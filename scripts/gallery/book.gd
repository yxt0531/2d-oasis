extends Control

var displayed_language
var current_message
var language_textures = []

func _ready():
	displayed_language = Settings.current_language
	current_message = 0
	
	match(displayed_language):
		Settings.Language.ENGLISH:
			language_textures.append(load("res://assets/textures/messages/message_english_01.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_02.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_03.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_04.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_05.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_06.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_07.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_08.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_09.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_10.png"))
		Settings.Language.CHINESE:
			language_textures.append(load("res://assets/textures/messages/message_chinese_01.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_02.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_03.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_04.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_05.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_06.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_07.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_08.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_09.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_10.png"))
	
	$Texture.texture = language_textures[0]
	
func _process(delta):
	if visible:
		_process_message(delta)
		if Input.is_action_just_pressed("toggle_menu") or Input.is_action_just_pressed("hide_camera_menu"):
			visible = false
			
	_process_camera_tracking(delta)
	_process_language_change(delta)
	
func _process_message(_delta):
	if Input.is_action_just_pressed("ring_turn_right"):
		if current_message < 9:
			current_message += 1
		_show_message(current_message)
	elif Input.is_action_just_pressed("ring_turn_left"):
		if current_message > 0:
			current_message -= 1
		_show_message(current_message)
		
func _show_message(messaage_index):
	match messaage_index:
		0:
			$Texture.texture = language_textures[0]
		1:
			$Texture.texture = language_textures[1]
		2:
			$Texture.texture = language_textures[2]
		3:
			$Texture.texture = language_textures[3]
		4:
			$Texture.texture = language_textures[4]
		5:
			$Texture.texture = language_textures[5]
		6:
			$Texture.texture = language_textures[6]
		7:
			$Texture.texture = language_textures[7]
		8:
			$Texture.texture = language_textures[8]
		9:
			$Texture.texture = language_textures[9]

func _process_camera_tracking(_delta):
	rect_position.x = get_parent().get_node("Camera").get_camera_screen_center().round().x - 960
	
func _process_language_change(_delta):
	if displayed_language != Settings.current_language:
		displayed_language = Settings.current_language
		language_textures.clear()
		match(displayed_language):
			Settings.Language.ENGLISH:
				language_textures.append(load("res://assets/textures/messages/message_english_01.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_02.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_03.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_04.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_05.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_06.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_07.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_08.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_09.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_10.png"))
			Settings.Language.CHINESE:
				language_textures.append(load("res://assets/textures/messages/message_chinese_01.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_02.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_03.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_04.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_05.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_06.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_07.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_08.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_09.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_10.png"))
				
		match current_message:
			0:
				$Texture.texture = language_textures[0]
			1:
				$Texture.texture = language_textures[1]
			2:
				$Texture.texture = language_textures[2]
			3:
				$Texture.texture = language_textures[3]
			4:
				$Texture.texture = language_textures[4]
			5:
				$Texture.texture = language_textures[5]
			6:
				$Texture.texture = language_textures[6]
			7:
				$Texture.texture = language_textures[7]
			8:
				$Texture.texture = language_textures[8]
			9:
				$Texture.texture = language_textures[9]
		print("message language changed")
