extends Control

const HOVER_FADE_SPEED = 2

var buttons = []

func _ready():
	buttons.append($ButtonLanguageNormal/ButtonLanguage)
	buttons.append($ButtonExitNormal/ButtonExit)

	$CheckFullScreen.pressed = OS.get_borderless_window()
	$CheckVSync.pressed = OS.vsync_enabled

func _process(delta):
	_process_camera_tracking(delta)
	_process_button_hover_effect(delta)
	_process_menu(delta)

func _process_camera_tracking(_delta):
	rect_position.x = get_parent().get_node("Camera").get_camera_screen_center().round().x - 960

func _process_button_hover_effect(delta):
	for b in buttons:
		if b.is_hovered():
			if b.modulate.a < 1:
				b.modulate.a += delta * HOVER_FADE_SPEED
		elif b.modulate.a > 0:
			b.modulate.a -= delta * HOVER_FADE_SPEED

func _process_menu(_delta):
	if Input.is_action_just_pressed("toggle_menu") and not get_parent().get_node("Book").visible:
		visible = not visible
		MouseEmulator.reset_cursor_position()
		print("camera menu toggled")

	if visible:
		if Input.is_action_just_pressed("hide_camera_menu"):
			visible = false
			
		if Input.get_mouse_mode() != Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
		match Settings.current_language:
			Settings.Language.ENGLISH:
				$ButtonLanguageNormal/LabelButtonText.text = "Language"
				$ButtonExitNormal/LabelButtonText.text = "Return"
				
				$CheckFullScreen.text = "  Fullscreen"
				$CheckVSync.text = "  Vertical Sync"
				
				if InputTypeDetector.current_type == InputTypeDetector.Type.KEYBOARD:
					$LabelHelp.text = "Move: A, D\nChange Path: W, S\nPickup Memory Fragment: F\nRotate Ring: Q, E\nChange Active Ring: Tab\nReset Puzzle: R"
				elif InputTypeDetector.current_type == InputTypeDetector.Type.GAMEPAD:
					$LabelHelp.text = "Move: LS Left, Right\nChange Path: LS Up, Down\nPickup Memory Fragment: X\nRotate Ring: LB, RB\nChange Active Ring: Y\nReset Puzzle: Select"
			Settings.Language.CHINESE:
				$ButtonLanguageNormal/LabelButtonText.text = "切换语言"
				$ButtonExitNormal/LabelButtonText.text = "返回"
				
				$CheckFullScreen.text = "  全屏"
				$CheckVSync.text = "  垂直同步"
				
				if InputTypeDetector.current_type == InputTypeDetector.Type.KEYBOARD:
					$LabelHelp.text = "移动：A，D\n切换路径：W，S\n互动：F\n旋转：Q，E\n切换当前：TAB\n重置：R"
				elif InputTypeDetector.current_type == InputTypeDetector.Type.GAMEPAD:
					$LabelHelp.text = "移动：LS 左，右\n切换路径：LS 上，下\n互动：X\n旋转：LB，RB\n切换当前：Y\n重置：Select"
	else:
		if Input.get_mouse_mode() != Input.MOUSE_MODE_HIDDEN:
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
			
func _on_ButtonLanguage_button_down():
	if Settings.current_language == Settings.Language.ENGLISH:
		Settings.current_language = Settings.Language.CHINESE
	elif Settings.current_language == Settings.Language.CHINESE:
		Settings.current_language = Settings.Language.ENGLISH

func _on_CheckFullScreen_toggled(button_pressed):
	if button_pressed:
		GraphicsController.enable_borderless_fullscreen()
	else:
		GraphicsController.disable_borderless_fullscreen()

func _on_CheckVSync_toggled(button_pressed):
	if button_pressed:
		GraphicsController.enable_vsync()
	else:
		GraphicsController.disable_vsync()

func _on_ButtonExit_button_down():
	get_parent().get_node("AsyncLoader").async_load("res://scenes/main_menu_finish.tscn")
