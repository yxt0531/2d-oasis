extends Node

const MOVE_SENSITIVITY = 750

var init_resolution

var enabled
var mouse_viewport_pos: Vector2

func _ready():
	init_resolution = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
	print("init project window resolution: " + str(init_resolution))
	
	enable()

func _process(delta):
	if enabled:
		_process_joystick_mouse_movement(delta)
		_process_click(delta)

func _process_joystick_mouse_movement(delta):
	if _is_controller_movement_active():
		mouse_viewport_pos += delta * MOVE_SENSITIVITY * Vector2(Input.get_action_strength("joystick_mouse_right"), Input.get_action_strength("joystick_mouse_down"))
		mouse_viewport_pos -= delta * MOVE_SENSITIVITY * Vector2(Input.get_action_strength("joystick_mouse_left"), Input.get_action_strength("joystick_mouse_up"))
		get_viewport().warp_mouse(mouse_viewport_pos)
	else:
		mouse_viewport_pos = get_viewport().get_mouse_position()
	
func _process_click(_delta):
	if Input.is_action_just_pressed("joystick_mouse_left_click"):
		_left_click()
	elif Input.is_action_just_released("joystick_mouse_left_click"):
		_left_click(false)
		
func _is_controller_movement_active():
	if Input.is_action_pressed("joystick_mouse_up"):
		return true
	if Input.is_action_pressed("joystick_mouse_down"):
		return true
	if Input.is_action_pressed("joystick_mouse_left"):
		return true
	if Input.is_action_pressed("joystick_mouse_right"):
		return true
	return false

func _left_click(down = true):
	var event = InputEventMouseButton.new()
	
	var scaled_viewport_position = get_viewport().get_mouse_position() * (get_viewport().get_size() / init_resolution)
	# print("input event scaled viewport position: " + str(scaled_viewport_position))
	
	var black_border_window_size = (OS.window_size - get_viewport().get_size()) / 2
	# print("black border window size: " + str(black_border_window_size))
	
	event.button_index = BUTTON_LEFT
	event.position = scaled_viewport_position + black_border_window_size
	if down:
		event.pressed = true
		Input.parse_input_event(event)
	else:
		event.pressed = false
		Input.parse_input_event(event)
	
	get_viewport().warp_mouse(mouse_viewport_pos)
	
	# print("joystick mouse left clicked")

func enable():
	if not enabled:
		mouse_viewport_pos = get_viewport().get_mouse_position()
		enabled = true
		print("mouse emulator enabled")

func disable():
	if enabled:
		enabled = false
		print("mouse emulator disabled")

func reset_cursor_position():
	mouse_viewport_pos = init_resolution / 2
	get_viewport().warp_mouse(mouse_viewport_pos)
