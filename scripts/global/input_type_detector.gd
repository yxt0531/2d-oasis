extends Node

enum Type { GAMEPAD, KEYBOARD }

const MOTION_DEADZONE = 0.2

var current_type

func _ready():
	current_type = Type.KEYBOARD

func _input(event):
	if event is InputEventJoypadButton:
		current_type = Type.GAMEPAD
		# print("gamepad input detected")
	elif event is InputEventJoypadMotion and event.axis_value > MOTION_DEADZONE:
		current_type = Type.GAMEPAD
		# print("gamepad input detected")
	elif event is InputEventKey:
		current_type = Type.KEYBOARD
		# print("keyboard input detected")
