extends Node

enum ProgressState { INIT, CONTINUE, FINISH }
enum Language { ENGLISH, CHINESE }

var current_progress_state
var current_language
var current_level

# for saving
const SAVE_PATH = "user://settings.save"
var save_buffer = {}
var save_file

func _ready():
	save_file = File.new()
	_load()

func _exit_tree():
	save()

func _process(_delta):
	if Input.is_action_just_pressed("toggle_fullscreen"):
		GraphicsController.toggle_borderless_fullscreen()

func _load():
	if save_file.file_exists(SAVE_PATH):
		save_file.open(SAVE_PATH, File.READ)
		save_buffer = parse_json(save_file.get_line())
		save_file.close()
		
		current_progress_state = int(save_buffer.current_progress_state)
		current_language = int(save_buffer.current_language)
		current_level = int(save_buffer.current_level)
		
		if save_buffer.borderless:
			GraphicsController.enable_borderless_fullscreen()
		else:
			GraphicsController.disable_borderless_fullscreen()
			
		if save_buffer.vsync:
			GraphicsController.enable_vsync()
		else:
			GraphicsController.disable_vsync()
	else:
		# load default
		current_progress_state = ProgressState.INIT
		current_language = Language.ENGLISH
		current_level = 0
		
		GraphicsController.enable_borderless_fullscreen()
		GraphicsController.enable_vsync()

func save():
	save_buffer.current_progress_state = current_progress_state
	save_buffer.current_language = current_language
	save_buffer.current_level = current_level
	
	save_buffer.borderless = OS.get_borderless_window()
	save_buffer.vsync = OS.vsync_enabled
	
	save_file.open(SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(save_buffer))
	save_file.close()

func reset_progress():
	current_progress_state = ProgressState.INIT
	current_level = 0
	
