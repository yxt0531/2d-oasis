extends Node

func find_node_by_name(root, name): # find node from root by name
	if(root.get_name() == name):
		print("node found: " + name)
		return root
		
	for child in root.get_children():
		if(child.get_name() == name):
			print("node found: " + name)
			return child
		
		var found = find_node_by_name(child, name)
		if(found):
			print("node found: " + name)
			return found
	return null
