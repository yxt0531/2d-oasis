extends Node

func _ready():
	center_window()

func toggle_borderless_fullscreen():
	if not OS.get_borderless_window():
		enable_borderless_fullscreen()
	else:
		disable_borderless_fullscreen()

func enable_borderless_fullscreen():
	if not OS.get_borderless_window():
		OS.set_borderless_window(true)
		OS.set_window_size(OS.get_screen_size())
		OS.set_window_position(Vector2(0, 0))
		print("borderless fullscreen enabled")

func disable_borderless_fullscreen():
	if OS.get_borderless_window():
		OS.set_borderless_window(false)
		center_window()
		OS.set_window_maximized(true)
		print("borderless fullscreen disabled")
	else:
		center_window()
		OS.set_window_maximized(true)
		
func toggle_vsync():
	if OS.vsync_enabled:
		disable_vsync()
	else:
		enable_vsync()

func enable_vsync():
	OS.vsync_enabled = true
	print("vsync enabled")
	
func disable_vsync():
	OS.vsync_enabled = false
	print("vsync disabled")

func center_window():
	OS.set_window_position((OS.get_screen_size() - OS.get_window_size()) / 2)
	if OS.get_screen_size() < OS.get_window_size():
		OS.set_window_size(OS.get_screen_size())
		OS.set_window_position(Vector2(0, 0))
	print("window centered")
