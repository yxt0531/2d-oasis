extends Node2D

func _ready():
	if Input.get_mouse_mode() != Input.MOUSE_MODE_HIDDEN:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		
	$AnimationPlayer.play("end_game")
	print("end game sequence started")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "end_game":
		Settings.current_progress_state = Settings.ProgressState.FINISH
		Settings.current_level = 0
		Settings.save()
		$AsyncLoader.async_load("res://scenes/main_menu_finish.tscn")
