extends Control

func _ready():
	if Global.DEBUG:
		get_tree().change_scene("res://scenes/debug/debug_main_menu.tscn")
	else:
		match Settings.current_progress_state:
			Settings.ProgressState.INIT:
				$AsyncLoader.async_load("res://scenes/main_menu_init.tscn")
			Settings.ProgressState.CONTINUE:
				$AsyncLoader.async_load("res://scenes/main_menu_continue.tscn")
			Settings.ProgressState.FINISH:
				$AsyncLoader.async_load("res://scenes/main_menu_finish.tscn")
