extends Node2D

const MIN_LEVEL = 0
const MAX_LEVEL = 10
const END_LEVEL = 10

const ENVIRONMENT_LOAD_DISTANCE = 1080
const END_GAME_LOAD_DISTANCE = 2160

var current_level
var level_environment_path = []
var level_environment = []
var hint_controller

func _init():
	Global.tower = self
	
	level_environment_path.append("res://system/world_environments/level_1.tres")
	level_environment_path.append("res://system/world_environments/level_2.tres")
	level_environment_path.append("res://system/world_environments/level_3.tres")
	level_environment_path.append("res://system/world_environments/level_4.tres")
	level_environment_path.append("res://system/world_environments/level_5.tres")
	level_environment_path.append("res://system/world_environments/level_6.tres")
	level_environment_path.append("res://system/world_environments/level_7.tres")
	level_environment_path.append("res://system/world_environments/level_8.tres")
	level_environment_path.append("res://system/world_environments/level_9.tres")
	
	for le_path in level_environment_path:
		level_environment.append(load(le_path))

	current_level = Settings.current_level

func _ready():
	hint_controller = $Camera/HintController
	if current_level == 0:
		next_level()

func _exit_tree():
	Settings.current_level = current_level
	print("current level saved to settings: " + str(Settings.current_level))

func _process(_delta):
	_process_level_activation(_delta)
	_process_world_environment_change(_delta)

func _process_level_activation(_delta):
	if not $Camera.is_moving():
		match current_level:
			1:
				$Background/Level1.activate()
			2:
				$Background/Level2.activate()
			3:
				$Background/Level3.activate()
			4:
				$Background/Level4.activate()
			5:
				$Background/Level5.activate()
			6:
				$Background/Level6.activate()
			7:
				$Background/Level7.activate()
			8:
				$Background/Level8.activate()
			9:
				$Background/Level9.activate()
	else:
		$Background/Level1.deactivate()
		$Background/Level2.deactivate()
		$Background/Level3.deactivate()
		$Background/Level4.deactivate()
		$Background/Level5.deactivate()
		$Background/Level6.deactivate()
		$Background/Level7.deactivate()
		$Background/Level8.deactivate()
		$Background/Level9.deactivate()

func _process_world_environment_change(_delta):
	match current_level:
		1:
			if $WorldEnvironment.environment != level_environment[0]:
				if ($Camera.position - $Background/Level1.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[0]
					print("level 1 environment loaded")
		2:
			if $WorldEnvironment.environment != level_environment[1]:
				if ($Camera.position - $Background/Level2.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[1]
					print("level 2 environment loaded")
		3:
			if $WorldEnvironment.environment != level_environment[2]:
				if ($Camera.position - $Background/Level3.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[2]
					print("level 3 environment loaded")
		4:
			if $WorldEnvironment.environment != level_environment[3]:
				if ($Camera.position - $Background/Level4.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[3]
					print("level 4 environment loaded")
		5:
			if $WorldEnvironment.environment != level_environment[4]:
				if ($Camera.position - $Background/Level5.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[4]
					print("level 5 environment loaded")
		6:
			if $WorldEnvironment.environment != level_environment[5]:
				if ($Camera.position - $Background/Level6.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[5]
					print("level 6 environment loaded")
		7:
			if $WorldEnvironment.environment != level_environment[6]:
				if ($Camera.position - $Background/Level7.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[6]
					print("level 7 environment loaded")
		8:
			if $WorldEnvironment.environment != level_environment[7]:
				if ($Camera.position - $Background/Level8.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[7]
					print("level 8 environment loaded")
		9:
			if $WorldEnvironment.environment != level_environment[8]:
				if ($Camera.position - $Background/Level9.position).length() < ENVIRONMENT_LOAD_DISTANCE:
					$WorldEnvironment.environment = level_environment[8]
					print("level 9 environment loaded")
		10:
			# end game trigger
			if ($Camera.position - $Background/ColorRectLevels/Level10.rect_position).length() < END_GAME_LOAD_DISTANCE:
				$AsyncLoader.rect_position.x = $Camera.position.x - 960
				$AsyncLoader.rect_position.y = $Camera.position.y - 540
				if not $AsyncLoader.get_progress("res://scenes/end_game.tscn") >= 0:
					$AsyncLoader.async_load("res://scenes/end_game.tscn")
				
func is_valid_level(level: int):
	return (level >= MIN_LEVEL and level <= MAX_LEVEL)
	
func is_end_level():
	return current_level == END_LEVEL
	
func next_level():
	if is_valid_level(current_level + 1) and $Camera.move_up():
		current_level += 1
		print("proceeding to new level: " + str(current_level))
	else:
		print("cannot proceed to next level: " + str(current_level + 1))
	
func prev_level():
	if is_valid_level(current_level - 2) and $Camera.move_down():
		current_level -= 1
		print("proceeding to new level: " + str(current_level))
	else:
		print("cannot proceed to previous level: " + str(current_level - 1))
		
