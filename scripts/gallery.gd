extends Node2D

const PLAYER_MOVE_SPEED = 400
const CAMERA_LIMIT_LEFT = -824
const CAMERA_LIMIT_RIGHT = 824
const PLAYER_CAMERA_ZONE = 720

var is_active: bool

var player_sprite
var player_area
var current_player_path_follow

var player_previous_global_position_x
var player_previous_global_position_y

func _ready():
	is_active = true

	player_sprite = $PlayerPath/PlayerPathFollow/PlayerSprite
	player_area = $PlayerPath/PlayerPathFollow/PlayerSprite/PlayerArea
	current_player_path_follow = $PlayerPath/PlayerPathFollow

	player_previous_global_position_x = player_sprite.position.x
	player_previous_global_position_y = player_sprite.position.y

func _process(delta):
	if is_active:
		_process_camera_tracking(delta)
		_process_player_movement(delta)
		_process_player_interact(delta)
		_process_player_animation(delta)

func _process_camera_tracking(_delta):
	if ($Camera.position.x - player_sprite.to_global(player_sprite.position).x) > PLAYER_CAMERA_ZONE:
		$Camera.position.x -= ($Camera.position.x - player_sprite.to_global(player_sprite.position).x) - PLAYER_CAMERA_ZONE
	elif ($Camera.position.x - player_sprite.to_global(player_sprite.position).x) < -PLAYER_CAMERA_ZONE:
		$Camera.position.x += (-($Camera.position.x - player_sprite.to_global(player_sprite.position).x) - PLAYER_CAMERA_ZONE)

func _process_player_movement(delta):
	# update player previous position
	player_previous_global_position_x = player_sprite.to_global(player_sprite.position).x
	player_previous_global_position_y = player_sprite.to_global(player_sprite.position).y
	
	if Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		current_player_path_follow.offset += delta * PLAYER_MOVE_SPEED
	elif Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		current_player_path_follow.offset -= delta * PLAYER_MOVE_SPEED

func _process_player_interact(_delta):
	for o_area in $BookTriggerArea.get_overlapping_areas():
		if o_area.name == player_area.name:
			if Input.is_action_just_pressed("player_interact"):
				$Book.visible = true

func _process_player_animation(_delta):
	if is_active: # prevent last frame changes
		if player_previous_global_position_x != player_sprite.to_global(player_sprite.position).x:
			if player_sprite.animation != "walking":
				player_sprite.play("walking")
		elif player_sprite.animation == "walking":
			player_sprite.play("idle")

	if Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		player_sprite.flip_h = true
	elif Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		player_sprite.flip_h = false
	elif Input.is_action_pressed("player_move_down"):
		pass
	elif Input.is_action_pressed("player_move_up"):
		if player_sprite.animation == "idle":
			player_sprite.play("back")
			
	if not is_active:
		if player_sprite.animation == "walking":
			player_sprite.play("idle") # prevent animation keep running after level finished


func _on_ButtonExit_button_down():
	pass # Replace with function body.
