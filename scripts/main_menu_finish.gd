extends Node2D

const HOVER_FADE_SPEED = 2

var buttons = []

func _ready():
	buttons.append($ButtonNewGameNormal/ButtonNewGame)
	buttons.append($ButtonGalleryNormal/ButtonGallery)
	buttons.append($ButtonExitGameNormal/ButtonExitGame)
	
	buttons.append($NewGameScreen/ButtonYesNormal/ButtonYes)
	buttons.append($NewGameScreen/ButtonCancelNormal/ButtonCancel)
	
	match Settings.current_language:
		Settings.Language.ENGLISH:
			if Settings.current_level == 0:
				$ButtonNewGameNormal/LabelButtonText.text = "New Game"
			else:
				$ButtonNewGameNormal/LabelButtonText.text = "Resume"
				
			$ButtonGalleryNormal/LabelButtonText.text = "Gallery"
			$ButtonExitGameNormal/LabelButtonText.text = "Exit Game"
			
			$NewGameScreen/LabelConfirm.text = "start a new game will reset all current progress"
			$NewGameScreen/ButtonYesNormal/LabelButtonText.text = "New Game"
			$NewGameScreen/ButtonCancelNormal/LabelButtonText.text = "Cancel"
		Settings.Language.CHINESE:
			if Settings.current_level == 0:
				$ButtonNewGameNormal/LabelButtonText.text = "新游戏"
			else:
				$ButtonNewGameNormal/LabelButtonText.text = "继续游戏"
				
			$ButtonGalleryNormal/LabelButtonText.text = "画廊"
			$ButtonExitGameNormal/LabelButtonText.text = "退出游戏"
			
			$NewGameScreen/LabelConfirm.text = "新游戏将重置当前所有进度"
			$NewGameScreen/ButtonYesNormal/LabelButtonText.text = "新游戏"
			$NewGameScreen/ButtonCancelNormal/LabelButtonText.text = "取消"

	if Input.get_mouse_mode() != Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
func _process(delta):
	_process_button_hover_effect(delta)
	
func _process_button_hover_effect(delta):
	for b in buttons:
		if b.is_hovered():
			if b.modulate.a < 1:
				b.modulate.a += delta * HOVER_FADE_SPEED
		elif b.modulate.a > 0:
			b.modulate.a -= delta * HOVER_FADE_SPEED

func _on_ButtonNewGame_button_down():
	if Settings.current_level == 0:
		$NewGameScreen.visible = true
	else:
		$AsyncLoader.async_load("res://scenes/tower.tscn")
		_hide_all_elements()

func _on_ButtonGallery_button_down():
	$AsyncLoader.async_load("res://scenes/gallery.tscn")
	_hide_all_elements()
	
func _on_ButtonExitGame_button_down():
	get_tree().quit()

func _on_ButtonYes_button_down():
	$AsyncLoader.async_load("res://scenes/tower.tscn")
	_hide_all_elements()

func _on_ButtonCancel_button_down():
	$NewGameScreen.visible = false

func _hide_all_elements():
	$ButtonNewGameNormal.visible = false
	$ButtonGalleryNormal.visible = false
	$ButtonExitGameNormal.visible = false
