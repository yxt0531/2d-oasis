extends Sprite

var eng_msg = preload("res://assets/textures/messages/message_english_10.png")
var chs_msg = preload("res://assets/textures/messages/message_chinese_10.png")

func _ready():
	match Settings.current_language:
		Settings.Language.ENGLISH:
			texture = eng_msg
		Settings.Language.CHINESE:
			texture = chs_msg
