extends Control

var camera
var bgm_player

func _ready():
	if Global.DEBUG:
		camera = Utils.find_node_by_name(get_parent(), "Camera")
		bgm_player = Utils.find_node_by_name(get_parent(), "BGMPlayer")
	else:
		queue_free()

func _process(_delta):
	if Input.is_action_just_released("toggle_debug"):
		visible = not visible
	
	if visible: # only update when visable
		rect_position.x = camera.position.x - 960
		rect_position.y = camera.position.y - 540
		$LabelCurrentLevel.text = "Camera Current Level: " + str(camera.current_level)
		$LabelFPS.text = "FPS: " + str(Engine.get_frames_per_second())
		$LabelCameraPosition.text = "Camera Position: " + str(camera.position)
		$LabelBGMVolume.text = "BGM Volume: " + str(bgm_player.volume_db) 
	
		for c_rect in $BGMVisualiser.get_children():
			c_rect.rect_size.y = 200 * bgm_player.get_visualiser_channel_db(int(c_rect.name))

func _on_ButtonToggleVSync_button_down():
	GraphicsController.toggle_vsync()

func _on_ButtonBackToMainMenu_button_down():
	get_parent().get_node("AsyncLoader").async_load("res://scenes/debug/debug_main_menu.tscn")

func _on_ButtonNextLevel_button_down():
	get_parent().next_level()
