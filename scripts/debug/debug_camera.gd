extends Camera2D

const ZOOM_SPEED = 0.1
const MOVE_SPEED = 100

var original_camera
var zoom_scale
var current_position

func _ready():
	if not Global.DEBUG:
		queue_free()
	
	original_camera = Utils.find_node_by_name(get_parent(), "Camera")
	
	zoom_scale = 1
	current_position = position

func _process(delta):
	if Input.is_action_just_pressed("toggle_debug_camera") and original_camera != null:
		current_position = original_camera.position
		if not current:
			original_camera.current = false
			current = true
		else:
			current = false
			original_camera.current = true
	
	if current:
		if Input.is_action_pressed("debug_camera_zoom_in"):
			zoom_scale -= delta * ZOOM_SPEED
		elif Input.is_action_pressed("debug_camera_zoom_out"):
			zoom_scale += delta * ZOOM_SPEED
		zoom = Vector2(1, 1) * zoom_scale
		
		if Input.is_action_pressed("debug_camera_move_left"):
			current_position.x -= delta * MOVE_SPEED
		elif Input.is_action_pressed("debug_camera_move_right"):
			current_position.x += delta * MOVE_SPEED
			
		if Input.is_action_pressed("debug_camera_move_up"):
			current_position.y -= delta * MOVE_SPEED
		elif Input.is_action_pressed("debug_camera_move_down"):
			current_position.y += delta * MOVE_SPEED
			
	position = current_position.round()
	force_update_scroll()
