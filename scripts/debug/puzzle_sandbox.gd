extends Control

const RING_ROTATION_SPEED = 180
const RING_ROTATION_STEP = 30

var puzzle_solved

var ring_1_rotation_degree
var ring_2_rotation_degree
var ring_3_rotation_degree
var ring_4_rotation_degree

func _ready():
	puzzle_solved = false
	ring_1_rotation_degree = int($PuzzleRings/PuzzleRing1.rotation_degrees)
	ring_2_rotation_degree = int($PuzzleRings/PuzzleRing2.rotation_degrees)
	ring_3_rotation_degree = int($PuzzleRings/PuzzleRing3.rotation_degrees)
	ring_4_rotation_degree = int($PuzzleRings/PuzzleRing4.rotation_degrees)

func _process(delta):
	_process_puzzle(delta)
	
func _process_puzzle(delta):
	if not puzzle_solved:
		# process ring rotation
		if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
		elif $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
				
		if $PuzzleRings/PuzzleRing2.rotation_degrees < ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing2.rotation_degrees > ring_2_rotation_degree:
				$PuzzleRings/PuzzleRing2.rotation_degrees = ring_2_rotation_degree
		elif $PuzzleRings/PuzzleRing2.rotation_degrees > ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing2.rotation_degrees < ring_2_rotation_degree:
				$PuzzleRings/PuzzleRing2.rotation_degrees = ring_2_rotation_degree
	
		if $PuzzleRings/PuzzleRing3.rotation_degrees < ring_3_rotation_degree:
			$PuzzleRings/PuzzleRing3.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing3.rotation_degrees > ring_3_rotation_degree:
				$PuzzleRings/PuzzleRing3.rotation_degrees = ring_3_rotation_degree
		elif $PuzzleRings/PuzzleRing3.rotation_degrees > ring_3_rotation_degree:
			$PuzzleRings/PuzzleRing3.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing3.rotation_degrees < ring_3_rotation_degree:
				$PuzzleRings/PuzzleRing3.rotation_degrees = ring_3_rotation_degree
				
		if $PuzzleRings/PuzzleRing4.rotation_degrees < ring_4_rotation_degree:
			$PuzzleRings/PuzzleRing4.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing4.rotation_degrees > ring_4_rotation_degree:
				$PuzzleRings/PuzzleRing4.rotation_degrees = ring_4_rotation_degree
		elif $PuzzleRings/PuzzleRing4.rotation_degrees > ring_4_rotation_degree:
			$PuzzleRings/PuzzleRing4.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing4.rotation_degrees < ring_4_rotation_degree:
				$PuzzleRings/PuzzleRing4.rotation_degrees = ring_4_rotation_degree
		# process ring rotation end
		
#		if _is_puzzle_solved():
#			puzzle_solved = true
#			print("puzzle_solved")

func _is_puzzle_solved():
	
	if puzzle_solved:
		return true
	else:
		if $PuzzleRings/PuzzleRing1.rotation_degrees != ring_1_rotation_degree:
			return false
		elif $PuzzleRings/PuzzleRing2.rotation_degrees != ring_2_rotation_degree:
			return false
		elif $PuzzleRings/PuzzleRing3.rotation_degrees != ring_3_rotation_degree:
			return false
		elif $PuzzleRings/PuzzleRing4.rotation_degrees != ring_4_rotation_degree:
			return false
		else:
			if int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360 != 0:
				return false
			elif int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360 != 0:
				return false
			elif int($PuzzleRings/PuzzleRing3.rotation_degrees) % 360 != 0:
				return false
			elif int($PuzzleRings/PuzzleRing4.rotation_degrees) % 360 != 0:
				return false
			else:
				return true

func _on_ButtonRing1Right_button_down():
	ring_1_rotation_degree += 30
	ring_2_rotation_degree += 30
	ring_3_rotation_degree += 60
	ring_4_rotation_degree += 90

func _on_ButtonRing1Left_button_down():
	ring_1_rotation_degree -= 30
	ring_2_rotation_degree -= 30
	ring_3_rotation_degree -= 60
	ring_4_rotation_degree -= 90

func _on_ButtonRing2Right_button_down():
	ring_2_rotation_degree += 30
	ring_1_rotation_degree -= 30
	ring_4_rotation_degree -= 30

func _on_ButtonRing2Left_button_down():
	ring_2_rotation_degree -= 30
	ring_1_rotation_degree -= 30
	ring_4_rotation_degree += 30

func _on_ButtonRing3Right_button_down():
	ring_3_rotation_degree += 30
	ring_2_rotation_degree -= 30

func _on_ButtonRing3Left_button_down():
	ring_3_rotation_degree -= 30
	ring_2_rotation_degree -= 30

func _on_ButtonRing4Right_button_down():
	ring_4_rotation_degree += 30
	ring_1_rotation_degree += 15
	ring_2_rotation_degree += 60
	ring_3_rotation_degree += 60
	
func _on_ButtonRing4Left_button_down():
	ring_4_rotation_degree -= 30
	ring_1_rotation_degree -= 15
	ring_2_rotation_degree -= 60
	ring_3_rotation_degree -= 60
