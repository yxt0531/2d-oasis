extends Control

func _ready():
	if Global.DEBUG:
		if Input.get_mouse_mode() != Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		queue_free()
		
func _process(_delta):
	if Input.is_action_just_pressed("toggle_debug"):
		visible = not visible
		
#	if get_parent().get_node("AsyncLoader").get_progress("res://scenes/tower.tscn") >= 0:
#		print(get_parent().get_node("AsyncLoader").get_progress("res://scenes/tower.tscn"))

func _on_ButtonLoadTower_button_down():
	if not get_parent().get_node("AsyncLoader").get_progress("res://scenes/tower.tscn") >= 0:
		get_parent().get_node("AsyncLoader").async_load("res://scenes/tower.tscn")

func _on_ButtonLoadLevel1_button_down():
	Settings.current_level = 1

func _on_ButtonLoadLevel2_button_down():
	Settings.current_level = 2

func _on_ButtonLoadLevel3_button_down():
	Settings.current_level = 3

func _on_ButtonLoadLevel4_button_down():
	Settings.current_level = 4

func _on_ButtonLoadLevel5_button_down():
	Settings.current_level = 5

func _on_ButtonLoadLevel6_button_down():
	Settings.current_level = 6

func _on_ButtonLoadLevel7_button_down():
	Settings.current_level = 7

func _on_ButtonLoadLevel8_button_down():
	Settings.current_level = 8

func _on_ButtonLoadLevel9_button_down():
	Settings.current_level = 9

func _on_ButtonSetLanguage_button_down():
	match Settings.current_language:
		Settings.Language.ENGLISH:
			Settings.current_language = Settings.Language.CHINESE
			$ButtonSetLanguage.text = "Current Language: CHS"
		Settings.Language.CHINESE:
			$ButtonSetLanguage.text = "Current Language: ENG"
			Settings.current_language = Settings.Language.ENGLISH

func _on_ButtonLoadInit_button_down():
	get_tree().change_scene("res://scenes/main_menu_init.tscn")

func _on_ButtonContinue_button_down():
	get_tree().change_scene("res://scenes/main_menu_continue.tscn")

func _on_ButtonFinish_button_down():
	get_tree().change_scene("res://scenes/main_menu_finish.tscn")
