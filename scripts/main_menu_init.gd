extends Control

const STEP_FADE_SPEED = 1
const HOVER_FADE_SPEED = 2
const AUTO_LOAD_TIME = 5

var current_step
var auto_load_timer
var buttons = []

func _ready():
	current_step = 1
	auto_load_timer = 0
	buttons.append($Step1/ButtonEnglishNormal/ButtonEnglish)
	buttons.append($Step1/ButtonChineseNormal/ButtonChinese)
	buttons.append($Step2/ButtonNextNormal/ButtonNext)
		
	$Step2/CheckFullScreen.pressed = OS.get_borderless_window()
	$Step2/CheckVSync.pressed = OS.vsync_enabled
	
	Settings.reset_progress()
	
	if Input.get_mouse_mode() != Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _process(delta):
	_process_button_hover_effect(delta)
	_process_step_change(delta)

func _process_button_hover_effect(delta):
	for b in buttons:
		if b.is_hovered():
			if b.modulate.a < 1:
				b.modulate.a += delta * HOVER_FADE_SPEED
		elif b.modulate.a > 0:
			b.modulate.a -= delta * HOVER_FADE_SPEED

func _process_step_change(delta):
	match current_step:
		1:
			$Step2.visible = false
			$Step3.visible = false
			
			$Step2.modulate.a = 0
			$Step3.modulate.a = 0
		2:
			if $Step1.modulate.a > 0:
				$Step1.modulate.a -= delta * STEP_FADE_SPEED
			elif $Step1.visible:
				$Step1.visible = false
			else:
				if not $Step2.visible:
					$Step2.visible = true
				elif $Step2.modulate.a < 1:
					$Step2.modulate.a += delta * STEP_FADE_SPEED
		3:
			if $Step2.modulate.a > 0:
				$Step2.modulate.a -= delta * STEP_FADE_SPEED
			elif $Step2.visible:
				$Step2.visible = false
			else:
				if not $Step3.visible:
					$Step3.visible = true
				elif $Step3.modulate.a < 1:
					$Step3.modulate.a += delta * STEP_FADE_SPEED
				else:
					auto_load_timer += delta
					if auto_load_timer >= AUTO_LOAD_TIME:
						current_step = 4
		4:
			if $Step3.modulate.a > 0:
				$Step3.modulate.a -= delta * STEP_FADE_SPEED
			elif $Step3.visible:
				$Step3.visible = false
			elif not $AsyncLoader.visible:
				print("start async loading")
				Settings.current_progress_state = Settings.ProgressState.CONTINUE
				$AsyncLoader.async_load("res://scenes/tower.tscn")

func _set_language(language):	
	match language:
		Settings.Language.ENGLISH:
			Settings.current_language = language
			$Step2/CheckFullScreen.text = "  Fullscreen"
			$Step2/CheckVSync.text = "  Vertical Sync"
			$Step2/ButtonNextNormal/LabelButtonText.text = "OK"
			
			$Step3/LabelController.text = "the game is best experienced with a controller"
			$Step3/LabelMenuSetting.text = "setting can be changed later with in-game menu"
			print("english language selected")
		Settings.Language.CHINESE:
			Settings.current_language = language
			$Step2/CheckFullScreen.text = "  全屏"
			$Step2/CheckVSync.text = "  垂直同步"
			$Step2/ButtonNextNormal/LabelButtonText.text = "确认"
			
			$Step3/LabelController.text = "推荐使用控制器进行游戏"
			$Step3/LabelMenuSetting.text = "设置可以稍后在游戏内菜单进行修改"
			print("chinese language selected")

func _on_ButtonEnglish_button_down():
	_set_language(Settings.Language.ENGLISH)
	current_step = 2

func _on_ButtonChinese_button_down():
	_set_language(Settings.Language.CHINESE)
	current_step = 2

func _on_CheckFullScreen_toggled(button_pressed):
	if button_pressed:
		GraphicsController.enable_borderless_fullscreen()
	else:
		GraphicsController.disable_borderless_fullscreen()

func _on_CheckVSync_toggled(button_pressed):
	if button_pressed:
		GraphicsController.enable_vsync()
	else:
		GraphicsController.disable_vsync()

func _on_Step2_ButtonNext_button_down():
	current_step = 3

func _on_Step3_ButtonNext_button_down():
	current_step = 4
