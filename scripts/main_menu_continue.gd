extends Control

const HOVER_FADE_SPEED = 2
const STEP_FADE_SPEED = 1

var current_step = 1

var buttons = []

func _ready():
	buttons.append($Step1/ButtonContinueNormal/ButtonContinue)
	buttons.append($Step1/ButtonRestartNormal/ButtonRestart)
	buttons.append($Step2/ButtonYesNormal/ButtonYes)
	buttons.append($Step2/ButtonNoNormal/ButtonNo)
	
	match Settings.current_language:
		Settings.Language.ENGLISH:
			$Step1/ButtonContinueNormal/LabelButtonText.text = "Resume"
			$Step1/ButtonRestartNormal/LabelButtonText.text = "New Game"
			
			$Step2/LabelConfirm.text = "start a new game will reset all current progress"
			$Step2/ButtonYesNormal/LabelButtonText.text = "New Game"
			$Step2/ButtonNoNormal/LabelButtonText.text = "Cancel"
		Settings.Language.CHINESE:
			$Step1/ButtonContinueNormal/LabelButtonText.text = "继续游戏"
			$Step1/ButtonRestartNormal/LabelButtonText.text = "新游戏"
			
			$Step2/LabelConfirm.text = "新游戏将重置当前所有进度"
			$Step2/ButtonYesNormal/LabelButtonText.text = "新游戏"
			$Step2/ButtonNoNormal/LabelButtonText.text = "返回"

	if Input.get_mouse_mode() != Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
func _process(delta):
	_process_button_hover_effect(delta)
	_process_step_change(delta)
		
func _process_button_hover_effect(delta):
	for b in buttons:
		if b.is_hovered():
			if b.modulate.a < 1:
				b.modulate.a += delta * HOVER_FADE_SPEED
		elif b.modulate.a > 0:
			b.modulate.a -= delta * HOVER_FADE_SPEED

func _process_step_change(delta):
	match current_step:
		1:
			if $Step2.modulate.a > 0:
				$Step2.modulate.a -= delta * STEP_FADE_SPEED
			elif $Step2.visible:
				$Step2.visible = false
			else:
				if not $Step1.visible:
					$Step1.visible = true
				elif $Step1.modulate.a < 1:
					$Step1.modulate.a += delta * STEP_FADE_SPEED
		2:
			if $Step1.modulate.a > 0:
				$Step1.modulate.a -= delta * STEP_FADE_SPEED
			elif $Step1.visible:
				$Step1.visible = false
			else:
				if not $Step2.visible:
					$Step2.visible = true
				elif $Step2.modulate.a < 1:
					$Step2.modulate.a += delta * STEP_FADE_SPEED
		3:
			$Step1.visible = false
			$Step2.visible = false
			
func _on_ButtonContinue_button_down():
	current_step = 3
	$AsyncLoader.async_load("res://scenes/tower.tscn")

func _on_ButtonRestart_button_down():
	current_step = 2

func _on_ButtonYes_button_down():
	current_step = 3
	Settings.reset_progress()
	$AsyncLoader.async_load("res://scenes/tower.tscn")

func _on_ButtonNo_button_down():
	current_step = 1
