extends Camera2D

enum State { NORMAL, BLACKED_OUT }

# TODO: needs to be adjusted in the future
const MOVING_SPEED = 160
const BLACKOUT_SPEED = 0.3

const RETURN_DISTANCE = 480 # distance of returning to normal state when moving and blacked out

var current_level
var current_state
var current_position: Vector2

func _ready():
	current_level = Global.tower.current_level
	current_state = State.BLACKED_OUT # default to blacked out state until fade in
	position.y = _get_level_target_position(current_level).y
	
	current_position = position
	
func _process(delta):
	_process_moving(delta)
	_process_blackout_fading(delta)
	
	position = current_position.round()
	force_update_scroll()

func _get_level_target_position(level: int):
	if Global.tower.is_valid_level(level):
		return Vector2(0, -1080 * 2 * level + 1080) # return new level position
	else:
		print("invalid level, cannot get level target position")
		return Vector2(0, -1080 * 2 * current_level + 1080) # return current level position

func _process_moving(delta):
	var target_level_position_y = _get_level_target_position(current_level).y
	
	if current_position.y > target_level_position_y:
		current_position.y -= delta * MOVING_SPEED
		if current_position.y < target_level_position_y:
			current_position.y = target_level_position_y # prevent overshoot
	elif current_position.y < target_level_position_y:
		current_position.y += delta * MOVING_SPEED
		if current_position.y > target_level_position_y:
			current_position.y = target_level_position_y # prevent overshoot

func _process_blackout_fading(delta):
	match current_state:
		State.BLACKED_OUT:
			if $BlackoutFadePlane.color.a < 1:
				$BlackoutFadePlane.color.a += delta * BLACKOUT_SPEED
				
		State.NORMAL:
			if $BlackoutFadePlane.color.a > 0:
				$BlackoutFadePlane.color.a -= delta * BLACKOUT_SPEED
			
		_:
			print("invalid state, cannot process blackout fading")

	var target_level_position_y = _get_level_target_position(current_level).y
	if abs(current_position.y - target_level_position_y) < RETURN_DISTANCE:
		_return_to_normal()

func _fade_in_black():
	if current_state != State.BLACKED_OUT:
		current_state = State.BLACKED_OUT
		print("camera state changed to blacked out")

func _return_to_normal():
	if current_state != State.NORMAL:
		current_state = State.NORMAL
		print("camera state returned to normal")

func is_moving():
	return not current_position.y == _get_level_target_position(current_level).y

func move_up():
	if Global.tower.is_valid_level(current_level + 1):
		if current_position.y == _get_level_target_position(current_level).y:
			current_level += 1
			_fade_in_black()
			print("camera moving up, target level: " + str(current_level))
			return true
		else:
			print("camera is currently moving")
			return false
	else:
		print("cannot move up, invalid target level: " + str(current_level + 1))
		return false
	
func move_down():
	if Global.tower.is_valid_level(current_level - 1):
		if current_position.y == _get_level_target_position(current_level).y:
			current_level -= 1
			_fade_in_black()
			print("camera moving down, target level: " + str(current_level))
			return true
		else:
			print("camera is currently moving")
			return false
	else:
		print("cannot move down, invalid target level: " + str(current_level - 1))
		return false
