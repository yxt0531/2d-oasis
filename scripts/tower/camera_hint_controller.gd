extends Control

enum ControlHint { MOVE, CHANGE_PATH, INTERACT, ROTATE_PUZZLE_RING, TOGGLE_RING, NEXT_LEVEL, RESET_PUZZLE, SHOW_MENU }

const HINT_FADE_SPEED = 0.5

var current_hint

func _ready():
	current_hint = null

func _process(delta):
	if not get_parent().is_moving():
		if not visible:
			visible = true
		_process_hint_display(delta)
	else:
		current_hint = null
		if visible:
			$LabelHint.modulate.a = 0
			visible = false

func _process_hint_display(delta):
	if current_hint == null:
		if $LabelHint.modulate.a > 0:
			$LabelHint.modulate.a -= delta * HINT_FADE_SPEED
		elif $LabelHint.modulate.a < 0:
			$LabelHint.modulate.a = 0
	else:
		if $LabelHint.modulate.a < 1:
			$LabelHint.modulate.a += delta * HINT_FADE_SPEED
		elif $LabelHint.modulate.a > 1:
			$LabelHint.modulate.a = 1
			
func display_hint(target_hint):
	if current_hint != target_hint:
		current_hint = target_hint
		print("current hint set to: " + str(current_hint))
	
	match Settings.current_language:
		Settings.Language.ENGLISH:
			if InputTypeDetector.current_type == InputTypeDetector.Type.KEYBOARD:
				# english keyboard hint
				match current_hint:
					ControlHint.MOVE:
						$LabelHint.text = "\"A\", \"D\", TO MOVE"
					ControlHint.CHANGE_PATH:
						$LabelHint.text = "\"W\", \"S\", TO CHANGE PATH"
					ControlHint.INTERACT:
						$LabelHint.text = "\"F\", TO PICKUP MEMORY FRAGMENT"
					ControlHint.ROTATE_PUZZLE_RING:
						$LabelHint.text = "MOVE CLOSER TO RING, USE \"Q\", \"E\", TO ROTATE"
					ControlHint.TOGGLE_RING:
						$LabelHint.text = "\"TAB\", TO CHANGE ACTIVE RING"
					ControlHint.NEXT_LEVEL:
						$LabelHint.text = "\"W\", TO PROCEED"
					ControlHint.RESET_PUZZLE:
						$LabelHint.text = "\"R\", TO RESET PUZZLE"
					ControlHint.SHOW_MENU:
						$LabelHint.text = "\"ESC\", TO OPEN MENU"
			elif InputTypeDetector.current_type == InputTypeDetector.Type.GAMEPAD:
				# english gamepad hint
				match current_hint:
					ControlHint.MOVE:
						$LabelHint.text = "\"LS LEFT\", \"RIGHT\", TO MOVE"
					ControlHint.CHANGE_PATH:
						$LabelHint.text = "\"LS UP\", \"DOWN\", TO CHANGE PATH"
					ControlHint.INTERACT:
						$LabelHint.text = "\"X\", TO PICKUP MEMORY FRAGMENT"
					ControlHint.ROTATE_PUZZLE_RING:
						$LabelHint.text = "MOVE CLOSER TO RING, USE \"LB\", \"RB\", TO ROTATE"
					ControlHint.TOGGLE_RING:
						$LabelHint.text = "\"Y\", TO CHANGE ACTIVE RING"
					ControlHint.NEXT_LEVEL:
						$LabelHint.text = "\"LS UP\", TO PROCEED"
					ControlHint.RESET_PUZZLE:
						$LabelHint.text = "\"SELECT\", TO RESET PUZZLE"
					ControlHint.SHOW_MENU:
						$LabelHint.text = "\"START\", TO OPEN MENU"

		Settings.Language.CHINESE:
			if InputTypeDetector.current_type == InputTypeDetector.Type.KEYBOARD:
				# chinese keyboard hint
				match current_hint:
					ControlHint.MOVE:
						$LabelHint.text = "移动：A，D"
					ControlHint.CHANGE_PATH:
						$LabelHint.text = "切换路径：W，S"
					ControlHint.INTERACT:
						$LabelHint.text = "互动：F"
					ControlHint.ROTATE_PUZZLE_RING:
						$LabelHint.text = "靠近后，使用Q，E旋转"
					ControlHint.TOGGLE_RING:
						$LabelHint.text = "切换当前：TAB"
					ControlHint.NEXT_LEVEL:
						$LabelHint.text = "前进：W"
					ControlHint.RESET_PUZZLE:
						$LabelHint.text = "重置：R"
					ControlHint.SHOW_MENU:
						$LabelHint.text = "显示菜单：ESC"
			elif InputTypeDetector.current_type == InputTypeDetector.Type.GAMEPAD:
				# chinese gamepad hint
				match current_hint:
					ControlHint.MOVE:
						$LabelHint.text = "移动：LS 左，右"
					ControlHint.CHANGE_PATH:
						$LabelHint.text = "切换路径：LS 上，下"
					ControlHint.INTERACT:
						$LabelHint.text = "互动：X"
					ControlHint.ROTATE_PUZZLE_RING:
						$LabelHint.text = "靠近后，使用LB，RB旋转"
					ControlHint.TOGGLE_RING:
						$LabelHint.text = "切换当前：Y"
					ControlHint.NEXT_LEVEL:
						$LabelHint.text = "前进：LS 上"
					ControlHint.RESET_PUZZLE:
						$LabelHint.text = "重置：SELECT"
					ControlHint.SHOW_MENU:
						$LabelHint.text = "显示菜单：START"
						
func clear_hint():
	current_hint = null
