extends Control

const FADEIN_SPEED = 1
const FADEOUT_SPEED = 0.5

var language_textures = []

var current_level
var is_read
var displayed_language

func _ready():
	current_level = -1
	is_read = true
	displayed_language = Settings.current_language
	
	match(displayed_language):
		Settings.Language.ENGLISH:
			language_textures.append(load("res://assets/textures/messages/message_english_01.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_02.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_03.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_04.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_05.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_06.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_07.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_08.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_09.png"))
			language_textures.append(load("res://assets/textures/messages/message_english_10.png"))
		Settings.Language.CHINESE:
			language_textures.append(load("res://assets/textures/messages/message_chinese_01.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_02.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_03.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_04.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_05.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_06.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_07.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_08.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_09.png"))
			language_textures.append(load("res://assets/textures/messages/message_chinese_10.png"))
	
func _process(delta):
	_process_message(delta)
	_process_language_change(delta)
	
func _process_message(delta):
	# TODO: messages need to be loaded into memory on ready
	if get_parent().is_moving() and $Texture.modulate.a < 1 and not Global.tower.is_end_level():
		if current_level != Global.tower.current_level:
			# load message texture according to level
			match Global.tower.current_level:
				1:
					$Texture.texture = language_textures[0]
				2:
					$Texture.texture = language_textures[1]
				3:
					$Texture.texture = language_textures[2]
				4:
					$Texture.texture = language_textures[3]
				5:
					$Texture.texture = language_textures[4]
				6:
					$Texture.texture = language_textures[5]
				7:
					$Texture.texture = language_textures[6]
				8:
					$Texture.texture = language_textures[7]
				9:
					$Texture.texture = language_textures[8]
			current_level = Global.tower.current_level
			is_read = false
			print("message texture loaded")
		$Texture.modulate.a += delta * FADEIN_SPEED
	elif not get_parent().is_moving() and $Texture.modulate.a > 0 and not is_read:
		if Input.is_action_pressed("player_move_up"):
			is_read = true
		elif Input.is_action_pressed("player_move_down"):
			is_read = true
		elif Input.is_action_pressed("player_move_left"):
			is_read = true
		elif Input.is_action_pressed("player_move_right"):
			is_read = true
		elif Input.is_action_pressed("player_interact"):
			is_read = true
	elif not get_parent().is_moving() and $Texture.modulate.a > 0:
		$Texture.modulate.a -= delta * FADEOUT_SPEED

func _process_language_change(_delta):
	if displayed_language != Settings.current_language:
		displayed_language = Settings.current_language
		language_textures.clear()
		match(displayed_language):
			Settings.Language.ENGLISH:
				language_textures.append(load("res://assets/textures/messages/message_english_01.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_02.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_03.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_04.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_05.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_06.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_07.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_08.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_09.png"))
				language_textures.append(load("res://assets/textures/messages/message_english_10.png"))
			Settings.Language.CHINESE:
				language_textures.append(load("res://assets/textures/messages/message_chinese_01.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_02.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_03.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_04.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_05.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_06.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_07.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_08.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_09.png"))
				language_textures.append(load("res://assets/textures/messages/message_chinese_10.png"))
				
		match Global.tower.current_level:
			1:
				$Texture.texture = language_textures[0]
			2:
				$Texture.texture = language_textures[1]
			3:
				$Texture.texture = language_textures[2]
			4:
				$Texture.texture = language_textures[3]
			5:
				$Texture.texture = language_textures[4]
			6:
				$Texture.texture = language_textures[5]
			7:
				$Texture.texture = language_textures[6]
			8:
				$Texture.texture = language_textures[7]
			9:
				$Texture.texture = language_textures[8]
		print("message language changed")
