extends AudioStreamPlayer

const FADEOUT_SPEED = 12
const FADEIN_SPEED = 48

var bgm_path = []
var bgm_stream = []

var current_playing

# visualiser related variables
var spectrum

var definition

var min_freq
var max_freq

var max_db
var min_db

var accel
var histogram = []
# visualiser related variables end

func _ready():
	bgm_path.append("res://assets/sounds/bgm/level_1.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_2.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_3.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_4.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_5.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_6.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_7.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_8.ogg")
	bgm_path.append("res://assets/sounds/bgm/level_9.ogg")

	for b_path in bgm_path:
		bgm_stream.append(load(b_path))
	
	current_playing = -1
	
	# visualiser related variables
	spectrum = AudioServer.get_bus_effect_instance(0, 0) # spectrum analyzer from master bus
	
	definition = 20

	min_freq = 20
	max_freq = 20000
	
	max_db = 0
	min_db = -36
	
	accel = 20
	for _i in range(definition):
		histogram.append(0)
	# visualiser related variables end

func _process(delta):
	_process_music_change(delta)
	_process_visualiser(delta)
	
func _process_music_change(delta):
	if Global.tower.current_level >= 1:
		if current_playing != Global.tower.current_level and volume_db > -48:
			volume_db -= delta * FADEOUT_SPEED
		elif current_playing != Global.tower.current_level and volume_db <= -48 and not Global.tower.is_end_level():
			stop()
			current_playing = Global.tower.current_level
			stream = bgm_stream[Global.tower.current_level - 1]
			play()
			print("playing bgm: " + str(current_playing))
		elif current_playing == Global.tower.current_level and volume_db < 0:
			volume_db += delta * FADEIN_SPEED
			if volume_db > 0:
				volume_db = 0

func _process_visualiser(delta):
	var freq = min_freq
	var interval = (max_freq - min_freq) / definition
	for i in range(definition):
		var freqrange_low = float(freq - min_freq) / float(max_freq - min_freq)
		freqrange_low = freqrange_low * freqrange_low * freqrange_low * freqrange_low
		freqrange_low = lerp(min_freq, max_freq, freqrange_low)
		
		freq += interval
		
		var freqrange_high = float(freq - min_freq) / float(max_freq - min_freq)
		freqrange_high = freqrange_high * freqrange_high * freqrange_high * freqrange_high
		freqrange_high = lerp(min_freq, max_freq, freqrange_high)
		
		var mag = spectrum.get_magnitude_for_frequency_range(freqrange_low, freqrange_high)
		mag = linear2db(mag.length())
		mag = (mag - min_db) / (max_db - min_db)
		
		mag += 0.3 * (freq - min_freq) / (max_freq - min_freq)
		mag = clamp(mag, 0.05, 1)
		
		histogram[i] = lerp(histogram[i], mag, accel * delta)

func get_visualiser_channel_db(channel: int):
	if channel >= 0 and channel < definition:
		return histogram[channel]
	else:
		return 0
