extends Node2D

const MEMORY_FRAGMENT_MOVE_SPEED = 0.25
const MEMORY_FRAGMENT_ROTATION_SPEED = 15
const LIGHT_FADE_SPEED = 0.5
const RING_SELECTION_LIGHT_DIM = 0.25 # minimum energy for selected ring alpha modulation
const RING_ROTATION_SPEED = 90
const PLAYER_MOVE_SPEED = 125
const GARGOYLE_EYE_MOVEMENT_RANGE = 6

var is_active

var bgm_player
var memory_fragment_aquired
var memory_fragment_activated
var puzzle_ring_activated
var puzzle_ring_selection_light_increasing # for ring selection highlight, should not be modified manually
var current_selected_puzzle_ring
var current_ray_length
var puzzle_solved

var ring_1_rotation_degree: int
var ring_2_rotation_degree: int

var player_sprite
var player_area
var current_player_path_follow

var player_previous_global_position_x
var player_previous_global_position_y

func _ready():
	is_active = false
	
	bgm_player = Global.tower.get_node("BGMPlayer")
	memory_fragment_aquired = false
	memory_fragment_activated = false
	puzzle_ring_selection_light_increasing = false
	puzzle_ring_activated = false
	current_selected_puzzle_ring = 0
	current_ray_length = 0
	puzzle_solved = false
	
	ring_1_rotation_degree = 90
	ring_2_rotation_degree = 180
	
	player_sprite = $PlayerPath/PlayerPathFollow/PlayerSprite
	player_area = $PlayerPath/PlayerPathFollow/PlayerSprite/PlayerArea
	current_player_path_follow = $PlayerPath/PlayerPathFollow
	
	player_previous_global_position_x = player_sprite.position.x
	player_previous_global_position_y = player_sprite.position.y

func _process(delta):
	if is_active:
		_process_lights(delta)
		_process_gargoyle_eyes(delta)
		_process_memory_fragment_animation(delta)
		_process_puzzle_ring_activation(delta)
		_process_puzzle_ring_selection_light(delta)
		_process_ray_length(delta)
		_process_puzzle(delta)
		
		_process_player_movement(delta)
		_process_player_interact(delta)
		_process_player_animation(delta)
		_process_hint(delta)

var debug_init_light_energy_printed = false

func _process_lights(_delta):
	# light flash control, magic number
	$LightBack/LightBright.energy = (bgm_player.get_visualiser_channel_db(1) * 0.75) + 0.5
	$LightFront/LightBright.energy = (bgm_player.get_visualiser_channel_db(1) * 0.75) + 0.5
	if not debug_init_light_energy_printed:
		print("init light energy: " + str($LightBack/LightBright.energy))
		debug_init_light_energy_printed = true
	
func _process_gargoyle_eyes(delta):
	if $GargoyleEyes.modulate.a < 1:
		$GargoyleEyes.modulate.a += delta * LIGHT_FADE_SPEED
	
	var player_local_pos = to_local(player_sprite.to_global(player_sprite.position))
	
	if $PlayerPath/PlayerPathFollow.get_child_count() > 0:
	# stop tracking after player x > 130
		if player_local_pos.x < 130:
			$GargoyleEyes/Eye1/Sprite.position = (player_local_pos - $GargoyleEyes/Eye1.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
			$GargoyleEyes/Eye2/Sprite.position = (player_local_pos - $GargoyleEyes/Eye2.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
			$GargoyleEyes/Eye3/Sprite.position = (player_local_pos - $GargoyleEyes/Eye3.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
			$GargoyleEyes/Eye4/Sprite.position = (player_local_pos - $GargoyleEyes/Eye4.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
	elif $PlayerPath2/PlayerPathFollow.get_child_count() > 0:
		$GargoyleEyes/Eye1/Sprite.position = (player_local_pos - $GargoyleEyes/Eye1.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
		$GargoyleEyes/Eye2/Sprite.position = (player_local_pos - $GargoyleEyes/Eye2.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
		$GargoyleEyes/Eye3/Sprite.position = (player_local_pos - $GargoyleEyes/Eye3.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE
		$GargoyleEyes/Eye4/Sprite.position = (player_local_pos - $GargoyleEyes/Eye4.position).normalized() * GARGOYLE_EYE_MOVEMENT_RANGE

func _process_memory_fragment_animation(delta):
	if memory_fragment_aquired:
		$MemoryFragmentPath/MemoryFragmentPathFollow/MemoryFragmentSprite.rotation_degrees += delta * MEMORY_FRAGMENT_ROTATION_SPEED
		if $MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset < 1:
			$MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset += delta * MEMORY_FRAGMENT_MOVE_SPEED
			if $MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset > 1:
				$MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset = 1
		elif not memory_fragment_activated:
			memory_fragment_activated = true
			print("memory_fragment_activated")

func _process_puzzle_ring_activation(delta):
	if memory_fragment_activated and not puzzle_ring_activated:
		if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 0.75:
			$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			if $PuzzleRings/PuzzleRing1/Activated.modulate.a > 0.75:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a = 0.75
				
		if $PuzzleRings/PuzzleRing2/Activated.modulate.a < 0.75:
			$PuzzleRings/PuzzleRing2/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			if $PuzzleRings/PuzzleRing2/Activated.modulate.a > 0.75:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a = 0.75
				
		if $PuzzleRings/PuzzleRing1/Activated.modulate.a >= 0.75 and $PuzzleRings/PuzzleRing2/Activated.modulate.a >= 0.75:
			puzzle_ring_activated = true
			current_ray_length = 1
			current_selected_puzzle_ring = 1
			print("puzzle_ring_activated, current ray length: " + str(current_ray_length))

func _process_puzzle_ring_selection_light(delta):
	if puzzle_ring_activated:
		# ring 1 process
		if current_selected_puzzle_ring == 1 and _is_player_in_ring_control_area():
			if puzzle_ring_selection_light_increasing:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing1/Activated.modulate.a > 0.75:
					puzzle_ring_selection_light_increasing = false
			else:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a -= delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing1/Activated.modulate.a < RING_SELECTION_LIGHT_DIM:
					puzzle_ring_selection_light_increasing = true
					
		else:
			if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 0.75:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			elif $PuzzleRings/PuzzleRing1/Activated.modulate.a > 0.75:
					$PuzzleRings/PuzzleRing1/Activated.modulate.a = 0.75
		# ring 1 process end
		
		# ring 2 process
		if current_selected_puzzle_ring == 2 and _is_player_in_ring_control_area():
			if puzzle_ring_selection_light_increasing:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a += delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing2/Activated.modulate.a > 0.75:
					puzzle_ring_selection_light_increasing = false
			else:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a -= delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing2/Activated.modulate.a < RING_SELECTION_LIGHT_DIM:
					puzzle_ring_selection_light_increasing = true
					
		else:
			if $PuzzleRings/PuzzleRing2/Activated.modulate.a < 0.75:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			elif $PuzzleRings/PuzzleRing2/Activated.modulate.a > 0.75:
					$PuzzleRings/PuzzleRing2/Activated.modulate.a = 0.75
		# ring 2 process end
		
func _process_ray_length(delta):
	if puzzle_ring_activated and current_ray_length > 0:
		$PuzzleRingRays/Ray1.visible = true
		$PuzzleRingRays/Ray2.visible = true
		$PuzzleRingRays/RayComplete.visible = true
		
		match current_ray_length:
			1:
				$PuzzleRingRays/Ray1.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/RayComplete.modulate.a -= delta * LIGHT_FADE_SPEED
			2:
				$PuzzleRingRays/Ray1.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/RayComplete.modulate.a -= delta * LIGHT_FADE_SPEED
			3:
				$PuzzleRingRays/RayComplete.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray1.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a -= delta * LIGHT_FADE_SPEED
		
		if $PuzzleRingRays/Ray1.modulate.a > 1:
			$PuzzleRingRays/Ray1.modulate.a = 1
		elif $PuzzleRingRays/Ray1.modulate.a < 0:
			$PuzzleRingRays/Ray1.modulate.a = 0
		
		if $PuzzleRingRays/Ray2.modulate.a > 1:
			$PuzzleRingRays/Ray2.modulate.a = 1
		elif $PuzzleRingRays/Ray2.modulate.a < 0:
			$PuzzleRingRays/Ray2.modulate.a = 0
		
		if $PuzzleRingRays/RayComplete.modulate.a > 1:
			$PuzzleRingRays/RayComplete.modulate.a = 1
		elif $PuzzleRingRays/RayComplete.modulate.a < 0:
			$PuzzleRingRays/RayComplete.modulate.a = 0

func _process_puzzle(delta):
	if puzzle_ring_activated and not puzzle_solved:
		if _is_player_in_ring_control_area():
			if Input.is_action_just_pressed("toggle_ring_selection"):
				if current_selected_puzzle_ring < 2:
					current_selected_puzzle_ring += 1
				else:
					current_selected_puzzle_ring = 1
				print("current selected ring: " + str(current_selected_puzzle_ring))
			elif Input.is_action_just_pressed("reset_rings"):
				$PuzzleRings/PuzzleRing1.rotation_degrees = int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360
				$PuzzleRings/PuzzleRing2.rotation_degrees = int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360
				ring_1_rotation_degree = 90
				ring_2_rotation_degree = 180
				print("reseting rings rotation")
			
			# puzzle logic start
			match current_selected_puzzle_ring:
				1:
					if Input.is_action_just_pressed("ring_turn_right") and not _is_ring_rotating():
						ring_1_rotation_degree += 30
						ring_2_rotation_degree += 30
					elif Input.is_action_just_pressed("ring_turn_left") and not _is_ring_rotating():
						ring_1_rotation_degree -= 30
						ring_2_rotation_degree += 30
				2:
					if Input.is_action_just_pressed("ring_turn_right") and not _is_ring_rotating():
						ring_2_rotation_degree += 30
						ring_1_rotation_degree += 60
					elif Input.is_action_just_pressed("ring_turn_left") and not _is_ring_rotating():
						ring_2_rotation_degree -= 30
						ring_1_rotation_degree -= 30
						
#			if Input.is_action_just_pressed("ring_turn_left") or Input.is_action_just_pressed("ring_turn_right"):
#				print(ring_1_rotation_degree)
#				print(ring_2_rotation_degree)
				
			# puzzle logic end
		
		# process ring rotation
		if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
		elif $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
				
		if $PuzzleRings/PuzzleRing2.rotation_degrees < ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing2.rotation_degrees > ring_2_rotation_degree:
				$PuzzleRings/PuzzleRing2.rotation_degrees = ring_2_rotation_degree
		elif $PuzzleRings/PuzzleRing2.rotation_degrees > ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing2.rotation_degrees < ring_2_rotation_degree:
				$PuzzleRings/PuzzleRing2.rotation_degrees = ring_2_rotation_degree
		# process ring rotation end
		
		# set rotation degree to 0 after 360 degree rotation
		if $PuzzleRings/PuzzleRing1.rotation_degrees == ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees = int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360
			ring_1_rotation_degree = int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360
			
		if $PuzzleRings/PuzzleRing2.rotation_degrees == ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees = int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360
			ring_2_rotation_degree = int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360
		# set rotation degree to 0 after 360 degree rotation end
		
		if not _is_ring_rotating():
			# only check result with rings are not rotating
			if int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360 != 0:
				current_ray_length = 1
			elif int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360 != 0:
				current_ray_length = 2
			elif int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360 == 0 and int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360 == 0:
				puzzle_solved = true
				print("puzzle_solved")
			
	elif puzzle_solved:
		current_selected_puzzle_ring = 0
		current_ray_length = 3
		
		if not $ExitDoor/ReceiverLight.visible:
			$ExitDoor/ReceiverLight.visible = true
		else:
			if $ExitDoor/ReceiverLight.modulate.a < 1.5:
				$ExitDoor/ReceiverLight.modulate.a += delta * LIGHT_FADE_SPEED
				if $ExitDoor/ReceiverLight.modulate.a > 1.5:
					$ExitDoor/ReceiverLight.modulate.a = 1.5
			if $ExitDoor/ReceiverLight.energy < 1.5:
				$ExitDoor/ReceiverLight.energy += delta * LIGHT_FADE_SPEED
				if $ExitDoor/ReceiverLight.energy > 1.5:
					$ExitDoor/ReceiverLight.energy = 1.5
		
		if not $ExitDoor.is_playing() and $ExitDoor/ReceiverLight.energy >= 1.5:
			$ExitDoor.play("open")

func _process_player_movement(delta):
	# update player previous position
	player_previous_global_position_x = player_sprite.to_global(player_sprite.position).x
	player_previous_global_position_y = player_sprite.to_global(player_sprite.position).y

	if player_area.get_overlapping_areas().size() > 0 and player_area.get_overlapping_areas()[0].collision_layer == 2:
		# allow change path only inside path switch area
		if Input.is_action_just_pressed("player_move_down"):
			if $PlayerPath2/PlayerPathFollow.get_child_count() == 0:
				$PlayerPath2/PlayerPathFollow.offset = $PlayerPath/PlayerPathFollow.offset
		
				$PlayerPath/PlayerPathFollow.remove_child(player_sprite)
				$PlayerPath2/PlayerPathFollow.add_child(player_sprite)
				
				current_player_path_follow = $PlayerPath2/PlayerPathFollow
			
		elif Input.is_action_just_pressed("player_move_up"):
			if $PlayerPath/PlayerPathFollow.get_child_count() == 0:
				$PlayerPath/PlayerPathFollow.offset = $PlayerPath2/PlayerPathFollow.offset
		
				$PlayerPath2/PlayerPathFollow.remove_child(player_sprite)
				$PlayerPath/PlayerPathFollow.add_child(player_sprite)
				
				current_player_path_follow = $PlayerPath/PlayerPathFollow
				
	if Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		current_player_path_follow.offset += delta * PLAYER_MOVE_SPEED
	elif Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		current_player_path_follow.offset -= delta * PLAYER_MOVE_SPEED

func _process_player_interact(_delta):
	if player_area.get_overlapping_areas().size() > 0 and player_area.get_overlapping_areas()[0].collision_layer == 4:
		# player entered fragment memory area
		if Input.is_action_just_pressed("player_interact") and not memory_fragment_aquired:
			memory_fragment_aquired = true
			print("memory fragment aquired")

func _process_player_animation(_delta):
	if is_active: # prevent last frame changes
		if player_previous_global_position_x != player_sprite.to_global(player_sprite.position).x:
			if player_sprite.animation != "walking":
				player_sprite.play("walking")
		elif player_sprite.animation == "walking":
			player_sprite.play("idle")

	if Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		player_sprite.flip_h = true
	elif Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		player_sprite.flip_h = false
	elif Input.is_action_pressed("player_move_down"):
		pass
	elif Input.is_action_pressed("player_move_up"):
		if player_sprite.animation == "idle":
			player_sprite.play("back")
			
	if not is_active and puzzle_solved:
		if player_sprite.animation == "walking":
			player_sprite.play("idle") # prevent animation keep running after level finished

func _process_hint(_delta):
	if $PathSwitchArea.get_overlapping_areas().size() > 0:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.CHANGE_PATH)
	elif $MemoryFragmentPath/MemoryFragmentPathFollow/MemoryFragmentSprite/MemoryFragmentArea.get_overlapping_areas().size() > 0:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.INTERACT)
	elif current_selected_puzzle_ring == 2 and not puzzle_solved:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.RESET_PUZZLE)
	elif $RingControlArea.get_overlapping_areas().size() > 0 and not puzzle_solved and puzzle_ring_activated:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.TOGGLE_RING)
	else:
		Global.tower.hint_controller.clear_hint()

func _is_player_in_ring_control_area():
	if player_area.get_overlapping_areas().size() > 0 and player_area.get_overlapping_areas()[0].collision_layer == 16:
		return true
	else:
		return false

func _is_ring_rotating():
	if $PuzzleRings/PuzzleRing1.rotation_degrees == ring_1_rotation_degree and $PuzzleRings/PuzzleRing2.rotation_degrees == ring_2_rotation_degree:
		return false
	else:
		return true

func _on_EntranceDoorCloseTrigger_area_entered(_area):
	if is_active:
		if not $EntranceDoor.is_playing() and $EntranceDoor.frame < 6:
			$EntranceDoor.play("close")
			
func _on_EndLevelTriggerArea_area_entered(_area):
	if is_active and puzzle_solved:
		deactivate()
		Global.tower.next_level()
	
func activate():
	if not is_active:
		is_active = true
		print("level 2 activated")

func deactivate():
	if is_active:
		is_active = false
		if player_sprite.animation == "walking":
			player_sprite.play("idle") # prevent animation keep running after level finished
		print("level 2 deactivated")
