extends Node2D

const MEMORY_FRAGMENT_MOVE_SPEED = 0.5
const MEMORY_FRAGMENT_ROTATION_SPEED = 15
const LIGHT_FADE_SPEED = 0.5
const RING_SELECTION_LIGHT_DIM = 0.35
const RING_ROTATION_SPEED = 90
const PLAYER_MOVE_SPEED = 200

var is_active

var bgm_player
var memory_fragment_aquired
var memory_fragment_activated
var puzzle_ring_activated
var puzzle_ring_selection_light_increasing
var current_ray_length
var puzzle_solved

var ring_1_rotation_degree

var player_sprite
var player_area
var current_player_path_follow

var player_previous_global_position_x
var player_previous_global_position_y

func _ready():
	is_active = false
	
	bgm_player = Global.tower.get_node("BGMPlayer")
	memory_fragment_aquired = false
	memory_fragment_activated = false
	puzzle_ring_activated = false
	puzzle_ring_selection_light_increasing = false
	current_ray_length = 0
	puzzle_solved = false
	
	ring_1_rotation_degree = -90 # init rotation
	
	player_sprite = $PlayerPath/PlayerPathFollow/PlayerSprite
	player_area = $PlayerPath/PlayerPathFollow/PlayerSprite/PlayerArea
	current_player_path_follow = $PlayerPath/PlayerPathFollow
	
	player_previous_global_position_x = player_sprite.position.x
	player_previous_global_position_y = player_sprite.position.y
	
func _process(delta):
	if is_active:
		_process_stars_light(delta)
		_process_memory_fragment_animation(delta)
		_process_puzzle_ring_activation(delta)
		_process_ray_length(delta)
		_process_puzzle(delta)
		_process_player_movement(delta)
		_process_player_interact(delta)
		_process_player_animation(delta)
		_process_hint(delta)
		
var debug_init_light_energy_printed = false
		
func _process_stars_light(_delta):
	$StarsLight.energy = (bgm_player.get_visualiser_channel_db(1) * 0.5) + 0.6 # light flash control, magic number
	if not debug_init_light_energy_printed:
		print("init light energy: " + str($StarsLight.energy))
		debug_init_light_energy_printed = true

func _process_memory_fragment_animation(delta):
	if memory_fragment_aquired:
		$MemoryFragmentPath/MemoryFragmentPathFollow/MemoryFragmentSprite.rotation_degrees += delta * MEMORY_FRAGMENT_ROTATION_SPEED
		if $MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset < 1:
			$MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset += delta * MEMORY_FRAGMENT_MOVE_SPEED
			if $MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset > 1:
				$MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset = 1
		elif not memory_fragment_activated:
			memory_fragment_activated = true
			print("memory_fragment_activated")

func _process_puzzle_ring_activation(delta):
	if memory_fragment_activated and not puzzle_ring_activated:
		if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 1:
			$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			if $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a = 1
		else:
			puzzle_ring_activated = true
			current_ray_length = 1
			print("puzzle_ring_activated, current ray length: " + str(current_ray_length))
	if memory_fragment_activated and $Lake/Lake.modulate.a < 1:
		if not $Lake/Lake.is_playing():
			$Lake/Lake.play()
		$Lake/Lake.modulate.a += delta * LIGHT_FADE_SPEED
		if $Lake/Lake.modulate.a > 1:
			$Lake/Lake.modulate.a = 1

func _process_ray_length(delta):
	if puzzle_ring_activated and current_ray_length > 0:
		$PuzzleRings/Ray1.visible = true
		$PuzzleRings/RayComplete.visible = true
		
		match current_ray_length:
			1:
				$PuzzleRings/Ray1.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRings/RayComplete.modulate.a -= delta * LIGHT_FADE_SPEED
			2:
				$PuzzleRings/RayComplete.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRings/Ray1.modulate.a -= delta * LIGHT_FADE_SPEED
		
		if $PuzzleRings/Ray1.modulate.a > 1:
			$PuzzleRings/Ray1.modulate.a = 1
		elif $PuzzleRings/Ray1.modulate.a < 0:
			$PuzzleRings/Ray1.modulate.a = 0
		
		if $PuzzleRings/RayComplete.modulate.a > 1:
			$PuzzleRings/RayComplete.modulate.a = 1
		elif $PuzzleRings/RayComplete.modulate.a < 0:
			$PuzzleRings/RayComplete.modulate.a = 0

func _process_puzzle(delta):
	if puzzle_ring_activated and not puzzle_solved:
		if _is_player_in_ring_control_area():
			if Input.is_action_just_pressed("ring_turn_right") and $PuzzleRings/PuzzleRing1.rotation_degrees == ring_1_rotation_degree:
				ring_1_rotation_degree += 30
				# $PuzzleRings/PuzzleRing1.rotation_degrees += 30
			elif Input.is_action_just_pressed("ring_turn_left") and $PuzzleRings/PuzzleRing1.rotation_degrees == ring_1_rotation_degree:
				ring_1_rotation_degree -= 30
				# $PuzzleRings/PuzzleRing1.rotation_degrees -= 30
				
			if puzzle_ring_selection_light_increasing:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
					puzzle_ring_selection_light_increasing = false
			else:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a -= delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing1/Activated.modulate.a < RING_SELECTION_LIGHT_DIM:
					puzzle_ring_selection_light_increasing = true
		else:
			if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 1:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			elif $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
					$PuzzleRings/PuzzleRing1/Activated.modulate.a = 1
		
		if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
		elif $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
		
		if int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360 == 0:
			puzzle_solved = true
			print("puzzle_solved")
			
	elif puzzle_solved:
		current_ray_length = 2
		
		if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 1:
			$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
		elif $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
			$PuzzleRings/PuzzleRing1/Activated.modulate.a = 1
			
		if not $ExitDoor/ReceiverLight.visible:
			$ExitDoor/ReceiverLight.visible = true
		else:
			if $ExitDoor/ReceiverLight.modulate.a < 1:
				$ExitDoor/ReceiverLight.modulate.a += delta * LIGHT_FADE_SPEED
				if $ExitDoor/ReceiverLight.modulate.a > 1:
					$ExitDoor/ReceiverLight.modulate.a = 1
			if $ExitDoor/ReceiverLight.energy < 1:
				$ExitDoor/ReceiverLight.energy += delta * LIGHT_FADE_SPEED
				if $ExitDoor/ReceiverLight.energy > 1:
					$ExitDoor/ReceiverLight.energy = 1
		
		if not $ExitDoor.is_playing() and $ExitDoor/ReceiverLight.energy >= 1:
			$ExitDoor.play("open")

func _process_player_movement(delta):
	# update player previous position
	player_previous_global_position_x = player_sprite.to_global(player_sprite.position).x
	player_previous_global_position_y = player_sprite.to_global(player_sprite.position).y

	if player_area.get_overlapping_areas().size() > 0 and player_area.get_overlapping_areas()[0].collision_layer == 2:
		# allow change path only inside path switch area
		if Input.is_action_just_pressed("player_move_down"):
			if $PlayerPath2/PlayerPathFollow.get_child_count() == 0:
				$PlayerPath2/PlayerPathFollow.unit_offset = $PlayerPath/PlayerPathFollow.unit_offset
		
				$PlayerPath/PlayerPathFollow.remove_child(player_sprite)
				$PlayerPath2/PlayerPathFollow.add_child(player_sprite)
				
				current_player_path_follow = $PlayerPath2/PlayerPathFollow
			
		elif Input.is_action_just_pressed("player_move_up"):
			if $PlayerPath/PlayerPathFollow.get_child_count() == 0:
				$PlayerPath/PlayerPathFollow.unit_offset = $PlayerPath2/PlayerPathFollow.unit_offset
		
				$PlayerPath2/PlayerPathFollow.remove_child(player_sprite)
				$PlayerPath/PlayerPathFollow.add_child(player_sprite)
				
				current_player_path_follow = $PlayerPath/PlayerPathFollow
	
	if Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		current_player_path_follow.offset += delta * PLAYER_MOVE_SPEED
	elif Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		current_player_path_follow.offset -= delta * PLAYER_MOVE_SPEED
	
func _process_player_interact(_delta):
	if player_area.get_overlapping_areas().size() > 0 and player_area.get_overlapping_areas()[0].collision_layer == 4:
		# player entered fragment memory area
		if Input.is_action_just_pressed("player_interact") and not memory_fragment_aquired:
			memory_fragment_aquired = true
			print("memory fragment aquired")
	
	if $EndLevelTriggerArea.get_overlapping_areas().size() > 0 and puzzle_solved and $ExitDoor.frame >= 6:
		if Input.is_action_just_pressed("player_interact") or Input.is_action_just_pressed("player_move_up"):
			Global.tower.next_level()
			deactivate()
			print("current level ended")
	
func _process_player_animation(_delta):
	if is_active: # prevent last frame changes
		if player_previous_global_position_x != player_sprite.to_global(player_sprite.position).x:
			if player_sprite.animation != "walking":
				player_sprite.play("walking")
		elif player_sprite.animation == "walking":
			player_sprite.play("idle")

	if Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		player_sprite.flip_h = true
	elif Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		player_sprite.flip_h = false
	elif Input.is_action_pressed("player_move_down"):
		pass
	elif Input.is_action_pressed("player_move_up"):
		if player_sprite.animation == "idle":
			player_sprite.play("back")
			
	if not is_active and puzzle_solved:
		if player_sprite.animation == "walking":
			player_sprite.play("idle") # prevent animation keep running after level finished
	
func _process_hint(_delta):
	if puzzle_solved:
		if $EndLevelTriggerArea.get_overlapping_areas().size() <= 0:
			Global.tower.hint_controller.clear_hint()
		else:
			Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.NEXT_LEVEL)
	elif $EntraceDoor.frame < 6:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.MOVE)
	elif not memory_fragment_aquired and $MemoryFragmentPath/MemoryFragmentPathFollow/MemoryFragmentSprite/MemoryFragmentArea.get_overlapping_areas().size() > 0:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.INTERACT)
	elif memory_fragment_aquired and not puzzle_ring_activated:
		Global.tower.hint_controller.clear_hint()
	elif puzzle_ring_activated:
		Global.tower.hint_controller.display_hint(Global.tower.hint_controller.ControlHint.ROTATE_PUZZLE_RING)
	else:
		Global.tower.hint_controller.clear_hint()
		
func _on_EntraceDoorCloseTrigger_area_entered(_area):
	if not $EntraceDoor.is_playing() and $EntraceDoor.frame < 6:
		$EntraceDoor.play("close")

func _is_player_in_ring_control_area():
	if player_area.get_overlapping_areas().size() > 0 and player_area.get_overlapping_areas()[0].collision_layer == 16:
		return true
	else:
		return false
		
func activate():
	if not is_active:
		is_active = true
		$Lake/LakeBlack.play()
		print("level 1 activated")

func deactivate():
	if is_active:
		is_active = false
		print("level 1 deactivated")
