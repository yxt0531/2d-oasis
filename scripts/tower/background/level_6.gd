extends Node2D

const MEMORY_FRAGMENT_MOVE_SPEED = 0.4
const MEMORY_FRAGMENT_ROTATION_SPEED = 15
const LIGHT_FADE_SPEED = 0.5
const RING_SELECTION_LIGHT_DIM = 0.5 # minimum energy for selected ring alpha modulation
const RING_ROTATION_SPEED = 90
const PLAYER_MOVE_SPEED = 125
const MERMAID_FADE_SPEED = 0.24

var is_active: bool

var bgm_player
var memory_fragment_aquired: bool
var memory_fragment_activated: bool
var puzzle_ring_activated: bool
var puzzle_ring_selection_light_increasing: bool # for ring selection highlight, should not be modified manually
var current_selected_puzzle_ring: int
var current_ray_length: int
var puzzle_solved: bool

var ring_1_rotation_degree: int
var ring_2_rotation_degree: int
var ring_3_rotation_degree: int

var player_sprite
var player_area
var current_player_path_follow

var player_previous_global_position_x
var player_previous_global_position_y

func _ready():
	is_active = false
	
	bgm_player = Global.tower.get_node("BGMPlayer")
	memory_fragment_aquired = false
	memory_fragment_activated = false
	puzzle_ring_activated = false
	puzzle_ring_selection_light_increasing = false
	current_selected_puzzle_ring = 0
	current_ray_length = 0
	puzzle_solved = false
	
	ring_1_rotation_degree = 0
	ring_2_rotation_degree = 60
	ring_3_rotation_degree = 90
	
	player_sprite = $PlayerPath/PlayerPathFollow/PlayerSprite
	player_area = $PlayerPath/PlayerPathFollow/PlayerSprite/PlayerArea
	current_player_path_follow = $PlayerPath/PlayerPathFollow
	
	player_previous_global_position_x = player_sprite.position.x
	player_previous_global_position_y = player_sprite.position.y
	
	$Layer2.play()

func _process(delta):
	if is_active:
		_process_mermaids(delta)
		_process_memory_fragment_animation(delta)
		_process_puzzle_ring_activation(delta)
		_process_puzzle_ring_selection_light(delta)
		_process_ray_length(delta)
		_process_puzzle(delta)
		
		_process_player_movement(delta)
		_process_player_interact(delta)
		_process_player_animation(delta)

func _process_mermaids(delta):
	if $Mermaid1.modulate.a > 0:
		$Mermaid1.modulate.a -= delta * MERMAID_FADE_SPEED
	if $Mermaid2.modulate.a > 0:
		$Mermaid2.modulate.a -= delta * MERMAID_FADE_SPEED
	if $Mermaid3.modulate.a > 0:
		$Mermaid3.modulate.a -= delta * MERMAID_FADE_SPEED
	if $Mermaid4.modulate.a > 0:
		$Mermaid4.modulate.a -= delta * MERMAID_FADE_SPEED
		
	for o_area in $Mermaid1Area.get_overlapping_areas():
		if o_area.name == player_area.name:
			if $Mermaid1.modulate.a < 1:
				$Mermaid1.modulate.a += delta * MERMAID_FADE_SPEED * 2
	
	for o_area in $Mermaid2Area.get_overlapping_areas():
		if o_area.name == player_area.name:
			if $Mermaid2.modulate.a < 1:
				$Mermaid2.modulate.a += delta * MERMAID_FADE_SPEED * 2
				
	for o_area in $Mermaid3Area.get_overlapping_areas():
		if o_area.name == player_area.name:
			if $Mermaid3.modulate.a < 1:
				$Mermaid3.modulate.a += delta * MERMAID_FADE_SPEED * 2
				
	for o_area in $Mermaid4Area.get_overlapping_areas():
		if o_area.name == player_area.name:
			if $Mermaid4.modulate.a < 1:
				$Mermaid4.modulate.a += delta * MERMAID_FADE_SPEED * 2
				
func _process_memory_fragment_animation(delta):
	if memory_fragment_aquired:
		$MemoryFragmentPath/MemoryFragmentPathFollow/MemoryFragmentSprite.rotation_degrees += delta * MEMORY_FRAGMENT_ROTATION_SPEED
		if $MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset < 1:
			$MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset += delta * MEMORY_FRAGMENT_MOVE_SPEED
			if $MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset > 1:
				$MemoryFragmentPath/MemoryFragmentPathFollow.unit_offset = 1
		elif not memory_fragment_activated:
			memory_fragment_activated = true
			$MemoryFragmentPath.z_index = 1
			print("memory_fragment_activated")

func _process_puzzle_ring_activation(delta):
	if memory_fragment_activated and not puzzle_ring_activated:
		if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 1:
			$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			if $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a = 1
				
		if $PuzzleRings/PuzzleRing2/Activated.modulate.a < 1:
			$PuzzleRings/PuzzleRing2/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			if $PuzzleRings/PuzzleRing2/Activated.modulate.a > 1:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a = 1
				
		if $PuzzleRings/PuzzleRing3/Activated.modulate.a < 1:
			$PuzzleRings/PuzzleRing3/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			if $PuzzleRings/PuzzleRing3/Activated.modulate.a > 1:
				$PuzzleRings/PuzzleRing3/Activated.modulate.a = 1
				
		if $PuzzleRings/PuzzleRing1/Activated.modulate.a >= 1 and $PuzzleRings/PuzzleRing2/Activated.modulate.a >= 1 and $PuzzleRings/PuzzleRing3/Activated.modulate.a >= 1:
			puzzle_ring_activated = true
			current_ray_length = 1
			current_selected_puzzle_ring = 1
			print("puzzle_ring_activated, current ray length: " + str(current_ray_length))

func _process_puzzle_ring_selection_light(delta):
	if puzzle_ring_activated:
		# ring 1 process
		if current_selected_puzzle_ring == 1 and _is_player_in_ring_control_area():
			if puzzle_ring_selection_light_increasing:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
					puzzle_ring_selection_light_increasing = false
			else:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a -= delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing1/Activated.modulate.a < RING_SELECTION_LIGHT_DIM:
					puzzle_ring_selection_light_increasing = true
					
		else:
			if $PuzzleRings/PuzzleRing1/Activated.modulate.a < 1:
				$PuzzleRings/PuzzleRing1/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			elif $PuzzleRings/PuzzleRing1/Activated.modulate.a > 1:
					$PuzzleRings/PuzzleRing1/Activated.modulate.a = 1
		# ring 1 process end
		
		# ring 2 process
		if current_selected_puzzle_ring == 2 and _is_player_in_ring_control_area():
			if puzzle_ring_selection_light_increasing:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a += delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing2/Activated.modulate.a > 1:
					puzzle_ring_selection_light_increasing = false
			else:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a -= delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing2/Activated.modulate.a < RING_SELECTION_LIGHT_DIM:
					puzzle_ring_selection_light_increasing = true
					
		else:
			if $PuzzleRings/PuzzleRing2/Activated.modulate.a < 1:
				$PuzzleRings/PuzzleRing2/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			elif $PuzzleRings/PuzzleRing2/Activated.modulate.a > 1:
					$PuzzleRings/PuzzleRing2/Activated.modulate.a = 1
		# ring 2 process end

		# ring 3 process
		if current_selected_puzzle_ring == 3 and _is_player_in_ring_control_area():
			if puzzle_ring_selection_light_increasing:
				$PuzzleRings/PuzzleRing3/Activated.modulate.a += delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing3/Activated.modulate.a > 1:
					puzzle_ring_selection_light_increasing = false
			else:
				$PuzzleRings/PuzzleRing3/Activated.modulate.a -= delta * LIGHT_FADE_SPEED
				if $PuzzleRings/PuzzleRing3/Activated.modulate.a < RING_SELECTION_LIGHT_DIM:
					puzzle_ring_selection_light_increasing = true
					
		else:
			if $PuzzleRings/PuzzleRing3/Activated.modulate.a < 1:
				$PuzzleRings/PuzzleRing3/Activated.modulate.a += delta * LIGHT_FADE_SPEED
			elif $PuzzleRings/PuzzleRing3/Activated.modulate.a > 1:
					$PuzzleRings/PuzzleRing3/Activated.modulate.a = 1
		# ring 3 process end

func _process_ray_length(delta):
	if puzzle_ring_activated and current_ray_length > 0:
		match current_ray_length:
			1:
				$PuzzleRingRays/Ray1.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray3.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/RayComplete.modulate.a -= delta * LIGHT_FADE_SPEED
			2:
				$PuzzleRingRays/Ray1.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray3.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/RayComplete.modulate.a -= delta * LIGHT_FADE_SPEED
			3:
				$PuzzleRingRays/Ray1.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray3.modulate.a += delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/RayComplete.modulate.a -= delta * LIGHT_FADE_SPEED
			4:
				$PuzzleRingRays/Ray1.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray2.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/Ray3.modulate.a -= delta * LIGHT_FADE_SPEED
				$PuzzleRingRays/RayComplete.modulate.a += delta * LIGHT_FADE_SPEED
		
		if $PuzzleRingRays/Ray1.modulate.a > 1:
			$PuzzleRingRays/Ray1.modulate.a = 1
		elif $PuzzleRingRays/Ray1.modulate.a < 0:
			$PuzzleRingRays/Ray1.modulate.a = 0
		
		if $PuzzleRingRays/Ray2.modulate.a > 1:
			$PuzzleRingRays/Ray2.modulate.a = 1
		elif $PuzzleRingRays/Ray2.modulate.a < 0:
			$PuzzleRingRays/Ray2.modulate.a = 0
		
		if $PuzzleRingRays/Ray3.modulate.a > 1:
			$PuzzleRingRays/Ray3.modulate.a = 1
		elif $PuzzleRingRays/Ray3.modulate.a < 0:
			$PuzzleRingRays/Ray3.modulate.a = 0
		
		if $PuzzleRingRays/RayComplete.modulate.a > 1:
			$PuzzleRingRays/RayComplete.modulate.a = 1
		elif $PuzzleRingRays/RayComplete.modulate.a < 0:
			$PuzzleRingRays/RayComplete.modulate.a = 0

func _process_puzzle(delta):
	if puzzle_ring_activated and not puzzle_solved:
		if _is_player_in_ring_control_area():
			if Input.is_action_just_pressed("toggle_ring_selection"):
				if current_selected_puzzle_ring < 3:
					current_selected_puzzle_ring += 1
				else:
					current_selected_puzzle_ring = 1
				print("current selected ring: " + str(current_selected_puzzle_ring))
			elif Input.is_action_just_pressed("reset_rings"):
				$PuzzleRings/PuzzleRing1.rotation_degrees = int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360
				$PuzzleRings/PuzzleRing2.rotation_degrees = int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360
				$PuzzleRings/PuzzleRing3.rotation_degrees = int($PuzzleRings/PuzzleRing3.rotation_degrees) % 360
				ring_1_rotation_degree = 0
				ring_2_rotation_degree = 60
				ring_3_rotation_degree = 90
				print("reseting rings rotation")
			
			# puzzle logic start
			if not _is_ring_rotating():
				match current_selected_puzzle_ring:
					1:
						if Input.is_action_just_pressed("ring_turn_right"):
							ring_1_rotation_degree += 30
							ring_2_rotation_degree += 30
							ring_3_rotation_degree -= 60
						elif Input.is_action_just_pressed("ring_turn_left"):
							ring_1_rotation_degree -= 30
							ring_2_rotation_degree -= 30
							ring_3_rotation_degree += 60
					2:
						if Input.is_action_just_pressed("ring_turn_right"):
							ring_2_rotation_degree += 30
							ring_3_rotation_degree += 60
						elif Input.is_action_just_pressed("ring_turn_left"):
							ring_2_rotation_degree -= 30
							ring_3_rotation_degree += 30
					3:
						if Input.is_action_just_pressed("ring_turn_right"):
							ring_3_rotation_degree += 30
							ring_1_rotation_degree = 90
						elif Input.is_action_just_pressed("ring_turn_left"):
							ring_3_rotation_degree -= 30
							ring_2_rotation_degree = 90
			# puzzle logic end
		
		# process ring rotation
		if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
		elif $PuzzleRings/PuzzleRing1.rotation_degrees > ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing1.rotation_degrees < ring_1_rotation_degree:
				$PuzzleRings/PuzzleRing1.rotation_degrees = ring_1_rotation_degree
				
		if $PuzzleRings/PuzzleRing2.rotation_degrees < ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing2.rotation_degrees > ring_2_rotation_degree:
				$PuzzleRings/PuzzleRing2.rotation_degrees = ring_2_rotation_degree
		elif $PuzzleRings/PuzzleRing2.rotation_degrees > ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing2.rotation_degrees < ring_2_rotation_degree:
				$PuzzleRings/PuzzleRing2.rotation_degrees = ring_2_rotation_degree
				
		if $PuzzleRings/PuzzleRing3.rotation_degrees < ring_3_rotation_degree:
			$PuzzleRings/PuzzleRing3.rotation_degrees += delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing3.rotation_degrees > ring_3_rotation_degree:
				$PuzzleRings/PuzzleRing3.rotation_degrees = ring_3_rotation_degree
		elif $PuzzleRings/PuzzleRing3.rotation_degrees > ring_3_rotation_degree:
			$PuzzleRings/PuzzleRing3.rotation_degrees -= delta * RING_ROTATION_SPEED
			if $PuzzleRings/PuzzleRing3.rotation_degrees < ring_3_rotation_degree:
				$PuzzleRings/PuzzleRing3.rotation_degrees = ring_3_rotation_degree
		# process ring rotation end
		
		# set rotation degree to 0 after 360 degree rotation
		if $PuzzleRings/PuzzleRing1.rotation_degrees == ring_1_rotation_degree:
			$PuzzleRings/PuzzleRing1.rotation_degrees = int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360
			ring_1_rotation_degree = int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360
			
		if $PuzzleRings/PuzzleRing2.rotation_degrees == ring_2_rotation_degree:
			$PuzzleRings/PuzzleRing2.rotation_degrees = int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360
			ring_2_rotation_degree = int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360
			
		if $PuzzleRings/PuzzleRing3.rotation_degrees == ring_3_rotation_degree:
			$PuzzleRings/PuzzleRing3.rotation_degrees = int($PuzzleRings/PuzzleRing3.rotation_degrees) % 360
			ring_3_rotation_degree = int($PuzzleRings/PuzzleRing3.rotation_degrees) % 360
		# set rotation degree to 0 after 360 degree rotation end
		
		if not _is_ring_rotating():
			# only check result with rings are not rotating
			if int($PuzzleRings/PuzzleRing1.rotation_degrees) % 360 != 0:
				current_ray_length = 1
			elif int($PuzzleRings/PuzzleRing2.rotation_degrees) % 360 != 0:
				current_ray_length = 2
			elif int($PuzzleRings/PuzzleRing3.rotation_degrees) % 360 != 0:
				current_ray_length = 3
			else:
				puzzle_solved = true
				print("puzzle_solved")
			
	elif puzzle_solved:
		current_selected_puzzle_ring = 0
		current_ray_length = 4
		
		if $PuzzleRingRays/RayComplete.modulate.a >= 1:
			if not $ExitDoor/ReceiverLight.visible:
				$ExitDoor/ReceiverLight.visible = true
			else:
				if $ExitDoor/ReceiverLight.energy < 1:
					$ExitDoor/ReceiverLight.energy += delta * LIGHT_FADE_SPEED
					if $ExitDoor/ReceiverLight.energy > 1:
						$ExitDoor/ReceiverLight.energy = 1

		if not $ExitDoor.is_playing() and $ExitDoor/ReceiverLight.energy >= 1:
			$ExitDoor.play("open")

func _process_player_movement(delta):
	# update player previous position
	player_previous_global_position_x = player_sprite.to_global(player_sprite.position).x
	player_previous_global_position_y = player_sprite.to_global(player_sprite.position).y
	
	if Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		current_player_path_follow.offset += delta * PLAYER_MOVE_SPEED
	elif Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		current_player_path_follow.offset -= delta * PLAYER_MOVE_SPEED

func _process_player_interact(_delta):
	for o_area in player_area.get_overlapping_areas():
		if o_area.collision_layer == 4:
			# player entered fragment memory area
			if Input.is_action_just_pressed("player_interact") and not memory_fragment_aquired:
				memory_fragment_aquired = true
				print("memory fragment aquired")
	
	if $EndLevelTriggerArea.get_overlapping_areas().size() > 0 and puzzle_solved and $ExitDoor.frame >= 11:
		if Input.is_action_just_pressed("player_interact") or Input.is_action_just_pressed("player_move_up"):
			Global.tower.next_level()
			deactivate()
			print("current level ended")

func _process_player_animation(_delta):
	if is_active: # prevent last frame changes
		if player_previous_global_position_x != player_sprite.to_global(player_sprite.position).x:
			if player_sprite.animation != "walking":
				player_sprite.play("walking")
		elif player_sprite.animation == "walking":
			player_sprite.play("idle")

	if Input.is_action_pressed("player_move_left") and not Input.is_action_pressed("player_move_right"):
		player_sprite.flip_h = true
	elif Input.is_action_pressed("player_move_right") and not Input.is_action_pressed("player_move_left"):
		player_sprite.flip_h = false
	elif Input.is_action_pressed("player_move_down"):
		pass
	elif Input.is_action_pressed("player_move_up"):
		if player_sprite.animation == "idle":
			player_sprite.play("back")
			
	if not is_active and puzzle_solved:
		if player_sprite.animation == "walking":
			player_sprite.play("idle") # prevent animation keep running after level finished

func _is_player_in_ring_control_area():
	for o_area in $RingControlArea.get_overlapping_areas():
		if o_area.name == player_area.name:
			return true
	return false

func _is_ring_rotating():
	if $PuzzleRings/PuzzleRing1.rotation_degrees == ring_1_rotation_degree and $PuzzleRings/PuzzleRing2.rotation_degrees == ring_2_rotation_degree and $PuzzleRings/PuzzleRing3.rotation_degrees == ring_3_rotation_degree:
		return false
	else:
		return true

func activate():
	if not is_active:
		is_active = true
		print("level 6 activated")

func deactivate():
	if is_active:
		is_active = false
		if player_sprite.animation == "walking":
			player_sprite.play("idle") # prevent animation keep running after level finished
		print("level 6 deactivated")

func _on_EntranceDoorCloseTrigger_area_entered(area):
	if is_active:
		if area.name == player_area.name:
			if not $EntranceDoor.is_playing() and $EntranceDoor.frame < 10:
				$EntranceDoor.play("close")
